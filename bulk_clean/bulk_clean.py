import csv
import sys
import os
import logging
from google.cloud import storage
from google.cloud import bigquery

logger = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
logger.addHandler(out_hdlr)
logger.setLevel(logging.INFO)

csv_filename_prefix = 'prod_search'
img_bucket = 'zoro-images'
gcp_prefix = 'product/full/'


bq_query = "SELECT * from gcp-dw.product_cache_ods.ta_images where hero_image like '%,%'"

def search_img_bq():
    client = bigquery.Client()
    query = client.query(bq_query)
    result = query.result()
    return result

def rename_file_in_bucket(filename, new_filename):
    try:
        client = storage.Client()
        storage_client = storage.Client()
        bucket = client.get_bucket(img_bucket)
        blob = bucket.blob(f'{gcp_prefix}{filename}')
        bucket.rename_blob(blob, f'{gcp_prefix}{new_filename}')
    except Exception as e:
        logger.error(f'Exception {e} in rename_file_in_bucket')

def rename_file_in_bg(filename, new_filename):
    try:
        client = bigquery.Client()
        query = f"UPDATE gcp-dw.product_cache_ods.ta_images SET hero_image = '{new_filename}' WHERE hero_image = '{filename}'"
        logger.info(f'QUERY: {query}')
        query = client.query(bq_query)
        result = query.result()
        return result
    except Exception as e:
        logger.error(f'Exception {e} in rename_file_in_bg')

def bulk_clean():
    try:
        hits = search_img_bq()
        records = 0
        for hit in hits:
            process_record(hit)
            records +=1
            if records % 100 == 0:
                logger.info(f'processed {records} records')
    except Exception as e:
        logger.info(f'Exception here {e}')

def process_record(hit_dict):
    try:
        #logger.info(f'DEGUG HIT:{hit_dict}')
        original_image_name = hit_dict['hero_image']
        new_image_name = original_image_name.replace(',', '_')
        rename_file_in_bucket(original_image_name, new_image_name)
        #rename_file_in_bg(original_image_name, new_image_name)
    except KeyError:
        logger.error('Failure in lookup')
        return

if __name__ == "__main__":
    bulk_clean()
