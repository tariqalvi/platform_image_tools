import csv
import sys
import os
import re
import logging
import traceback
from google.cloud import storage
from google.cloud import bigquery

logger = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
logger.addHandler(out_hdlr)
logger.setLevel(logging.INFO)

csv_write_location = '/tmp/'
mapping_file_bucket = 'bulk_create_2'
csv_filename_prefix = 'prod_search'
gcp_prefix = f'gs://zoro-images/product/full/'
csv_max_lines = 19000
current_csv_index = 0
current_csv_lines = 0
current_csv_file = None

bq_query = "SELECT "\
           "i.hero_image AS heroImage,"\
           "p.zoro_no AS zoroNo,"\
           "p.short_description AS itemValue,"\
           "p.web_title AS title,"\
           "p.short_description AS shortDescription,"\
           "p.manufacturer_name as manufacturerName,"\
           "p.supplier_stock_no as supplierStockNo,"\
           "p.supplier_name as supplierName,"\
           "p.zoro_level_one_name as categoryL1_name,"\
           "p.zoro_level_one_code as categoryL1_path_code,"\
           "p.zoro_level_two_name as categoryL2_name,"\
           "p.zoro_level_two_code as categoryL2_path_code,"\
           "p.zoro_level_three_name as categoryL3_name,"\
           "p.zoro_level_three_code as categoryL3_path_code "\
           "FROM "\
           "(SELECT DISTINCT hero_image FROM gcp-dw.product_cache_ods.ta_images) i "\
           "LEFT JOIN "\
           "(SELECT DISTINCT "\
           "zoro_image_name,"\
           "grainger_image_name,"\
           "zoro_no,"\
           "short_description,"\
           "web_title,"\
           "zoro_level_one_name,"\
           "zoro_level_one_code,"\
           "zoro_level_two_name,"\
           "zoro_level_two_code,"\
           "zoro_level_three_name,"\
           "zoro_level_three_code,"\
           "manufacturer_name,"\
           "supplier_stock_no,"\
           "supplier_name,"\
           "is_public "\
           "FROM gcp-dw.zdp_dm.d_product) p "\
           "ON i.hero_image = p.grainger_image_name WHERE p.grainger_image_name IS NOT NULL AND p.is_public = true "\
           "UNION ALL "\
           "SELECT "\
           "i.hero_image AS heroImage,"\
           "p.zoro_no AS zoroNo,"\
           "p.short_description AS itemValue,"\
           "p.web_title AS title,"\
           "p.short_description AS shortDescription,"\
           "p.manufacturer_name as manufacturerName,"\
           "p.supplier_stock_no as supplierStockNo,"\
           "p.supplier_name as supplierName,"\
           "p.zoro_level_one_name as categoryL1_name,"\
           "p.zoro_level_one_code as categoryL1_path_code,"\
           "p.zoro_level_two_name as categoryL2_name,"\
           "p.zoro_level_two_code as categoryL2_path_code,"\
           "p.zoro_level_three_name as categoryL3_name,"\
           "p.zoro_level_three_code as categoryL3_path_code " \
           "FROM "\
           "(SELECT DISTINCT hero_image FROM gcp-dw.product_cache_ods.ta_images) i "\
           "LEFT JOIN "\
           "(SELECT DISTINCT "\
           "zoro_image_name,"\
           "grainger_image_name,"\
           "zoro_no,"\
           "short_description,"\
           "web_title,"\
           "zoro_level_one_name,"\
           "zoro_level_one_code,"\
           "zoro_level_two_name,"\
           "zoro_level_two_code,"\
           "zoro_level_three_name,"\
           "zoro_level_three_code,"\
           "manufacturer_name,"\
           "supplier_stock_no,"\
           "supplier_name,"\
           "is_public "\
           "FROM gcp-dw.zdp_dm.d_product) p "\
           "ON i.hero_image = p.zoro_image_name where p.zoro_image_name IS NOT NULL AND p.is_public = true"

def search_img_bq():
    client = bigquery.Client()
    query = client.query(bq_query)
    result = query.result()
    return result

def move_file_to_bucket(filename):
    client = storage.Client()
    storage_client = storage.Client()
    bucket = client.get_bucket(mapping_file_bucket)
    blob = bucket.blob(filename)
    blob.upload_from_filename(f'{csv_write_location}{filename}')
    os.remove(f'{csv_write_location}{filename}')

def get_strip_quotes(dict_item, field, default):
    if not isinstance(dict_item[field], str):
        return dict_item[field]
    elif not dict_item[field]:
        return default

    return re.sub("[^0-9a-zA-Z_\- ]+", "_", dict_item[field])

def is_unavailable_image(image):
        not_avail = {
            'NOTAVAIL.JPG',
            'ZKAIyMrw_.JPG',
            '41R418_AW01.JPG',
            'Z1pGsupcplx_.JPG',
            'No_Image_Needed.JPG',
            'ZKap4dFIHmIbQSQU.JPG',
            'PHOTO_COMING_SOON.JPG',
            'ZMTdfffqRYdkSpEfTP.JPG',
            'Z37IG0fo5oy.JPG',
            'ZKAIyMrw_JPG',
            'ZKap4dFIHmIbQSQU.JPG'
        }

        return image in not_avail

#def bulk_create(event, context):
def bulk_create():
    try:
        hits = search_img_bq()
        records = 0
        for hit in hits:
            process_image(hit)
            records +=1
            if records % 1000000 == 0:
                logger.info(f'downloaded {records} records')

        generate_csv(None)
    except Exception as e:
        logger.info(f'Exception here {e}')

def process_image(hit_dict):
    try:
        img = hit_dict['heroImage']
        if is_unavailable_image(img):
            return

        fname, ext = os.path.splitext(img)
        fname_clean = fname.replace(',', '_')

        info = {'image-uri': f'{gcp_prefix}{fname_clean}{ext}',
                'image-id': f'{fname_clean}{ext}',
                'product-set-id': 'zoro-products',
                'product-id': hit_dict['zoroNo'],
                'product-category': 'general-v1',
                'product-display-name': get_strip_quotes(hit_dict, 'title', ''),
                'labels': { 'description': get_strip_quotes(hit_dict, 'shortDescription', ''),
                            'categoryL1_name': get_strip_quotes(hit_dict, 'categoryL1_name', ''),
                            'categoryL1_path_code': get_strip_quotes(hit_dict, 'categoryL1_path_code', ''),
                            'categoryL2_name': get_strip_quotes(hit_dict, 'categoryL2_name', ''),
                            'categoryL2_path_code': get_strip_quotes(hit_dict, 'categoryL2_path_code', ''),
                            'categoryL3_name': get_strip_quotes(hit_dict, 'categoryL3_name', ''),
                            'categoryL3_path_code': get_strip_quotes(hit_dict, 'categoryL3_path_code', ''),
                            'item': get_strip_quotes(hit_dict, 'shortDescription', ''),
                            'manufacturer_name': get_strip_quotes(hit_dict, 'manufacturerName', ''),
                            'supplier_stock_num': get_strip_quotes(hit_dict, 'supplierStockNo', ''),
                            'supplier_name': get_strip_quotes(hit_dict, 'supplierName', '')}}


    except Exception as e:
        logger.error(f'Exception in process_image {e} {traceback.format_exc()}')

    generate_csv(info)

def generate_csv(info):
    global current_csv_file
    global current_csv_index
    global current_csv_lines

    try:
        if not info:
            if current_csv_file:
                current_csv_file.close()
                move_file_to_bucket(f'{csv_filename_prefix}_{current_csv_index}.csv')
                logger.info(f'Closed {csv_write_location}{csv_filename_prefix}_{current_csv_index}.csv with {current_csv_lines} lines')
            return

        if not current_csv_file:
            current_csv_index += 1
            current_csv_lines = 0
            current_csv_file = open(f'{csv_write_location}{csv_filename_prefix}_{current_csv_index}.csv', 'w')
            logger.info(f'Opened {csv_write_location}{csv_filename_prefix}_{current_csv_index}.csv')

        label_list = []
        for name, value in info["labels"].items():
            if value:
                label_list.append(f'{name}={value}')
        labels = ','.join(label_list)

        """ logger.info(f'"{info["image-uri"]}",'
                    f'{info["image-id"]},'
                    f'{info["product-set-id"]},'
                    f'{info["product-id"]},'
                    f'{info["product-category"]},'
                    f'{info["product-display-name"]},'
                    f'"{labels}",')
        raise SystemExit(f'EXITING EARLY. REMOVE') """

        current_csv_file.write(f'"{info["image-uri"]}",'
                               f'{info["image-id"]},'
                               f'{info["product-set-id"]},'
                               f'{info["product-id"]},'
                               f'{info["product-category"]},'
                               f'{info["product-display-name"]},'
                               f'"{labels}",\n')
        current_csv_lines += 1
        if current_csv_lines == csv_max_lines:
            current_csv_file.close()
            move_file_to_bucket(f'{csv_filename_prefix}_{current_csv_index}.csv')
            logger.info(f'Closed {csv_write_location}{csv_filename_prefix}_{current_csv_index}.csv with {current_csv_lines} lines')
            current_csv_file = None
    except Exception as e:
        logger.error(f'Exception in generate_csv {e}')

if __name__ == "__main__":
    bulk_create()
