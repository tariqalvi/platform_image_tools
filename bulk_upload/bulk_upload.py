import csv
import sys
import os
import logging
import time
from google.cloud import storage
from google.cloud import vision_v1
from google.cloud import vision
from google.cloud.vision_v1 import enums

logger = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
logger.addHandler(out_hdlr)
logger.setLevel(logging.INFO)

csv_bucket = 'bulk_create_2'
product_set_location = 'us-west1'
product_set = 'zoro-products'
project_id = 'zoro-platform-sb'

def purge_products_in_product_set(
        project_id, location, product_set_id, force):
    """Delete all products in a product set.
    Args:
        project_id: Id of the project.
        location: A compute region name.
        product_set_id: Id of the product set.
        force: Perform the purge only when force is set to True.
    """
    client = vision.ProductSearchClient()

    parent = client.location_path(
        project=project_id, location=location)

    product_set_purge_config = vision.types.ProductSetPurgeConfig(
        product_set_id=product_set_id)

    # The purge operation is async.
    operation = client.purge_products(
        parent=parent,
        product_set_purge_config=product_set_purge_config,
        # The operation is irreversible and removes multiple products.
        # The user is required to pass in force=True to actually perform the
        # purge.
        # If force is not set to True, the service raises an exception.
        force=force)

    operation.result(timeout=10000)

    logger.info('Deleted products in product set.')

def cleanup():
    client = vision.ProductSearchClient()
    try:
        location_path = client.location_path(project=project_id, location=product_set_location)
        product_sets = client.list_product_sets(parent=location_path)
        for product_set in product_sets:
            logger.info('Product set name: {}'.format(product_set.name))
            logger.info('Product set id: {}'.format(product_set.name.split('/')[-1]))
            logger.info('Product set display name: {}'.format(product_set.display_name))
            logger.info('Product set index time:')
            logger.info('  seconds: {}'.format(product_set.index_time.seconds))
            logger.info('  nanos: {}\n'.format(product_set.index_time.nanos))

        name = client.product_set_path(project_id, product_set_location, product_set)
        client.delete_product_set(name)
        logger.info('Product set deleted')
    except Exception as e:
        logger.error(f'Failed to delete product set! Error {e}')

def import_product_sets(project_id, location, gcs_uri):
        """Import images of different products in the product set.
        Args:
            project_id: Id of the project.
            location: A compute region name.
            gcs_uri: Google Cloud Storage URI.
                Target files must be in Product Search CSV format.
        """
        client = vision_v1.ProductSearchClient()
        
        # A resource that represents Google Cloud Platform location.
        location_path = client.location_path(
            project=project_id, location=location)

        # Set the input configuration along with Google Cloud Storage URI
        gcs_source = vision_v1.types.ImportProductSetsGcsSource(
            csv_file_uri=gcs_uri)
        input_config = vision_v1.types.ImportProductSetsInputConfig(
            gcs_source=gcs_source)

        # Import the product sets from the input URI.
        response = client.import_product_sets(
            parent=location_path, input_config=input_config)

        logger.info(f'Processing operation name: {response.operation.name} uri: {gcs_uri}')
        # synchronous check of operation status
        try:
            result = response.result()
        except Exception as e:
            logger.error(f'Incorrect arguments!')(f'Error {e} in result')
            return False
        
        logger.info(f'Processing done on {gcs_uri}')

        for i, status in enumerate(result.statuses):
            logger.info('Status of processing line {} of the csv: {}'.format(
                i, status))
            try:
                # Check the status of reference image
                # `0` is the code for OK in google.rpc.Code.
                if status.code == 0:
                    reference_image = result.reference_images[i]
                    logger.info(reference_image)
                else:
                    logger.error(f'Status code not OK: {status.message}')
            except Exception as e:
                logger.error(f'Exception {e} in result.reference_images[{i}] status code {status.code}')
        return True




def bulk_upload():
    #cleanup()
    storage_client = storage.Client()
    """ bucket = storage_client.get_bucket(csv_bucket)
    blobs = bucket.list_blobs()
    for blob in blobs:
        logger.info(f'gs://{csv_bucket}/{blob.name}')
        result = import_product_sets('zoro-platform-sb',
                                     product_set_location,
                                     f'gs://{csv_bucket}/{blob.name}') """
    for i in range(1, 148):
        blob_name = f'gs://{csv_bucket}/prod_search_{i}.csv'
        result = import_product_sets('zoro-platform-sb',
                                     product_set_location,
                                     blob_name)
        if not result:
            time.sleep(3)
            import_product_sets('zoro-platform-sb',
                                product_set_location,
                                blob_name)

if __name__ == "__main__":
    #cleanup()
    #purge_products_in_product_set(project_id, product_set_location, 'zoro-products', True)
    bulk_upload()
