from PIL import Image
import os
import sys

def iterate(path, minwidth, maxwidth, minheight, maxheight):
    print(f'Processing folder {path} with params minwidth:{minwidth} maxwidth:{maxwidth} minheight:{minheight} maxheight:{maxheight}')
    csv_file = open(f'size.csv', 'w')
    csv_file.write('Image,Width,Height,Valid\n')
    for filename in os.listdir(path):
        file, extension = os.path.splitext(filename)
        if extension.lower() == '.jpg' or extension.lower() == '.png':
            im = Image.open(f'{path}/{filename}')
            w, h = im.size
            valid = 'Yes'
            if w < minwidth or w > maxwidth or h < minheight or h > maxheight:
                valid = 'No'
            csv_file.write(f'{filename},{w},{h},{valid}\n')
    csv_file.close()
    print('Done!')

if __name__ == "__main__":
    if len(sys.argv) != 6:
        SystemExit('Args: directory minwidth maxwdith minheight maxheight!')
    iterate(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]))