import os
import sys
import signal
import json
import logging
import configparser
import itertools
import threading
from openpyxl import load_workbook
from json.decoder import JSONDecodeError
from google.cloud import pubsub_v1
from google.cloud import storage
from platform_utilities import platform_utils


class AtomicInteger():
    def __init__(self, value=0):
        self._value = value
        self._lock = threading.Lock()

    def inc(self):
        with self._lock:
            self._value += 1
            return self._value

    def dec(self):
        with self._lock:
            self._value -= 1
            return self._value


#num_forks = AtomicInteger()
#num_forks = 0
children = []
logger_server = None
global_config_file = None


def create_logger(log_name, do_console=True):
    logger = logging.getLogger(f'{log_name}_{os.getpid()}')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh = logging.FileHandler(f'{log_name}_{os.getpid()}.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    if do_console:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
    logger.info('STARTING LOGGER')
    return logger


def download_worksheet(bucket_name, supplier, output_file_name, logger):
    global logger_server
    client = storage.Client()
    try:
        bucket = client.get_bucket(bucket_name)
    except Exception as e:
        raise SystemExit(f'Failed to locate GCP bucket: {e}')

    try:
        file = f'{supplier}/Cef_output/{output_file_name}'
        blob = bucket.blob(file)
        local_file_name = f'{supplier}_{output_file_name}'
        if os.path.exists(local_file_name):
            logger.error(f'{local_file_name} has already been processed!')
            return None
        blob.download_to_filename(local_file_name)
        logger.info(f'Downloaded {local_file_name}.')
    except Exception as e:
        logger.error(f'Failed to download {file}: {e}')
        raise SystemExit(f'Failed to download config_local.ini: {e}')
    return local_file_name


def generate_cef_data(file):
    work_book = load_workbook(file)
    ws = work_book['Content Enrichment Form']
    columns = []
    max_col_index = 0
    for col_index, col in enumerate(ws.iter_cols()):
        col_hdr = ws.cell(2, col_index + 1).value
        if not col_hdr:
            break
        columns.append(col_hdr)
        max_col_index = col_index

    data = []
    for row_index, row in enumerate(ws.iter_rows()):
        if row_index > 1:
            col1_val = ws.cell(row_index+1, 1).value
            if not col1_val:
                break
            row_data = {}
            for col_index in range(max_col_index):
                row_data[columns[col_index]] = ws.cell(row_index+1, col_index+1).value
            data.append(row_data)
    return data


def generate_missing_images_data(file, supplier):
    work_book = load_workbook(file)

    supplier_id = ''
    if 'Content Enrichment Form' in work_book:
        ws = work_book['Content Enrichment Form']
        supplier_id = ws['A3'].value

    if 'Missing Images' in work_book:
        ws = work_book['Missing Images']
    elif 'Missing images' in work_book:
        ws = work_book['Missing images']
    else:
        return []

    data = []
    for row_index, row in enumerate(ws.iter_rows()):
        if row_index > 1:
            asset_name = ws.cell(row_index, 1).value
            stock_no = ws.cell(row_index, 3).value
            if not asset_name:
                break
            missing_image_row = {'supplier_short_name': supplier_id,
                                 'supplier_stock_num': stock_no,
                                 'asset': asset_name,
                                 'scrape_sites': '',
                                 'scrape_allow_expressions': ''
                                 }
            data.append(missing_image_row)
    return data

def get_storage_filenames(bucket_name, supplier, subfolder):
    client = storage.Client()
    prefix = f'{supplier}/{subfolder}/'
    files = []
    for blob in client.list_blobs(bucket_name, prefix=prefix):
        blob_file_name = blob.name[len(prefix):]
        if blob_file_name:
            files.append(blob_file_name)
    return files


def do_post_process(configuration, bucket_name, supplier, output_file_name, logger, image_path):
    ws_name = download_worksheet(bucket_name, supplier, output_file_name, logger)
    if not ws_name:
        return

    data = generate_cef_data(ws_name)
    missing_image_data = generate_missing_images_data(ws_name, supplier)
    try:
        pu = platform_utils(configuration, logger, image_path)
        pu.process_content_enrichment(data, missing_image_data)
    except Exception as e:
        logger.error(f'Error {e} in content enrichment')


def post_process(configuration, logger):
    try:
        supplier = configuration['default']['supplier']
        bucket_name = configuration['default']['bucket_name']
    except KeyError as e:
        SystemExit('Error in post_process {e}')

    output_file_name = None
    if 'output_file_name' in configuration['default']:
        output_file_name = configuration['default']['output_file_name']

    image_path = None
    if 'image_path' in configuration['default']:
        image_path = configuration['default']['image_path']

    if output_file_name and image_path:
        logger.info(f'Single batch processing {bucket_name}:{supplier}:{output_file_name}:{image_path}')
        return do_post_process(configuration, bucket_name, supplier, output_file_name, logger, image_path)

    files = get_storage_filenames(bucket_name, supplier, 'Cef_output')
    logger.info(f'Supplier processing {bucket_name}:{supplier}')
    for file in files:
        name, extension = os.path.splitext(file)
        if extension != '.xlsx':
            logger.error(f'Found unexpected file {file}')
            continue
        tokens = name.split('/')
        if len(tokens) > 0:
            batch_name = tokens[-1]
            if not batch_name.endswith('_out') or not len(batch_name) > 4:
                logger.error(f'Found unexpected file {file}')
                continue
            batch_name = batch_name[:len(batch_name) - 4]
            output_file_name = f'{batch_name}_out.xlsx'
            image_path = f'{supplier}/Images/{batch_name}'
            configuration.set('default', 'image_path', image_path)
            logger.info(f'...Batch {bucket_name}:{supplier}:{output_file_name}:{image_path}')
            do_post_process(configuration, bucket_name, supplier, output_file_name, logger, image_path)
            #print(batch_name)


def callback(message):
    #global num_forks
    global global_config_file
    message.ack()
    message_string = message.data.decode("utf-8")
    try:
        message_json = json.loads(message_string)
    except JSONDecodeError as e:
        logger_server.error(f'INVALID MESSAGE RECEIVED: {e.msg}')

    try:
        pid = os.fork()
        if pid == 0:
            try:
                supplier = message_json.get('supplier')
                bucket_name = message_json.get('bucket_name')
                output_file_name = message_json.get('output_file_name')
                image_path = message_json.get('image_path')
                config_instance = configparser.ConfigParser()
                config_instance.read(global_config_file)
                config_instance.set('default', 'supplier', supplier)
                config_instance.set('default', 'bucket_name', bucket_name)
                config_instance.set('default', 'output_file_name', output_file_name)
                config_instance.set('default', 'image_path', image_path)
                logger_server.info(
                    f'Launching handler for supplier {supplier}, bucket {bucket_name}, file {output_file_name} image path {image_path}')
                file_name, _ = os.path.splitext(output_file_name)
                logger_instance = create_logger(f'{supplier}_{file_name}', False)

                post_process(config_instance, logger_instance)
                logger_instance.info(f'Handler exiting for supplier {supplier}, bucket {bucket_name}, file {output_file_name} image path {image_path}')
                os.kill(os.getpid(), signal.SIGKILL)
                #num_forks.inc()
            except KeyError as e1:
                logger_server.error(f'Unexpected data in callback {message_json.dumps()} {e1}')
        else:
            children.append(pid)
            logger_server.info(f'Forked child {pid}')

    except OSError as e2:
        logger_server.error(f'Could not create a child process {e2}')



def start_listener(project_id, topic_id):
    #global num_forks
    logger_server.info(f'Starting listener on topic {topic_id}, project {project_id}')
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(project_id, topic_id)
    future = subscriber.subscribe(subscription_path, callback=callback)
    try:
        future.result()
    #except Exception as e:
    except KeyboardInterrupt:
        future.cancel()


def main(configuration):
    try:
        project_id = config['default']['project_id']
    except KeyError as e:
        logger_server(f'Project id not supplied - {e}')
        exit()

    try:
        topic_id = configuration['default']['topic_id']
        start_listener(project_id, topic_id)
        while len(children):
            pid, status = os.waitpid(children[0], 0)
            logger_server(f'Wait for child {pid} returned {status}')
    except KeyError as e:
        try:
            post_process(configuration, logger_server)
        except KeyError:
            SystemExit(f'Invalid config - {e}')


if __name__ == "__main__":
    if len(sys.argv) < 2:
        SystemExit('No config supplied')

    global_config_file = sys.argv[1]
    logger_server = create_logger('cef_post_processor')
    config = configparser.ConfigParser()
    config.read(global_config_file)
    main(config)
