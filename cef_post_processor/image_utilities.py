import json
import time
from google.protobuf.json_format import MessageToJson, MessageToDict
from google.cloud import vision_v1
from google.cloud.vision_v1 import enums


class image_utils:
    def __init__(self, config, logger, image_column_lst, image_names_set):
        self.image_names_set = image_names_set
        self.config = config
        self.logger = logger
        self.input_url = None
        if 'input_bucket' in config['annotation']:
            self.input_url = config['annotation']['input_bucket']
        
        self.batch_size = 16
        if 'batch_size' in config['annotation']:
            self.batch_size = min(self.batch_size, int(config['annotation']['batch_size']))

        self.max_imgs_per_min = 1800
        if 'max_imgs_per_min' in config['annotation']:
            self.max_imgs_per_min = min(self.max_imgs_per_min, int(config['annotation']['max_imgs_per_min']))

        self.max_imgs_per_min = self.max_imgs_per_min - (self.max_imgs_per_min % self.batch_size)

        self.features = [
                         {"type": enums.Feature.Type.LABEL_DETECTION},
                         {"type": enums.Feature.Type.WEB_DETECTION},
                         {"type": enums.Feature.Type.SAFE_SEARCH_DETECTION},
                         # {"type": enums.Feature.Type.OBJECT_LOCALIZATION},
                         {"type": enums.Feature.Type.PRODUCT_SEARCH},
                        ]
        self.vision_client = vision_v1.ImageAnnotatorClient()

        if 'product_search_project_id' not in config['default']:
            raise Exception('product_search_project_id not specified')

        if 'product_search_location' not in config['default']:
            raise Exception('product_search_location not specified')

        if 'product_search_product_set_id' not in config['default']:
            raise Exception('product_search_product_set_id not specified')

        if 'product_search_product_cat' not in config['default']:
            raise Exception('product_search_product_cat not specified')

        if 'product_search_max_records_to_process' not in config['default']:
            raise Exception('product_search_max_records_to_process not specified')

        self.image_column_lst = image_column_lst
        
        self.product_search_project_id = config['default']['product_search_project_id']
        self.product_search_location = config['default']['product_search_location']
        self.product_search_product_set_id = config['default']['product_search_product_set_id']
        self.product_search_product_cat = config['default']['product_search_product_cat']
        self.product_search_max_records_to_process = int(config['default']['product_search_max_records_to_process'])

    @staticmethod
    def get_item_value(container, path_to_item):
        path = path_to_item.split('/')
        for n, path_step in enumerate(path):
            if n == len(path) - 1:
                return container.get(path_step)
            else:
                container = container[path_step]

    def product_search(self, dict_image_to_id):
        bucket_name = self.config['default']['bucket_name']
        #supplier_name = self.config['default']['supplier']
        image_path = self.config['default']['image_path']
        if image_path[-1] != '/':
            image_path = image_path + '/'
        url = f'gs://{bucket_name}/{image_path}'

        rows = []
        for filename in dict_image_to_id.keys():
            responses = self.get_product_catalog_suggestions(f'{url}{filename}')
            try:
                respcount = 0
                for response in responses['product_search_results']['results']:
                    respcount += 1
                    row = {'images_id' : dict_image_to_id[filename]}
                    row['image'] = response['image']
                    row['score'] = response.get('score')
                    row['name'] = self.get_item_value(response,
                                                    'product/name')
                    row['display_name'] = self.get_item_value(response,
                                                    'product/display_name')

                    product_labels = self.get_item_value(response,
                                                    'product/product_labels')
                    for label in product_labels:
                        value = label['value']
                        value = value.strip('"')
                        value = value.strip("'")
                        if value.isnumeric():
                            numeric_value = int(value)
                            row[label['key']] = numeric_value if numeric_value < 10000000 else 0
                        else:
                            row[label['key']] = value

                    if 'categoryL1_name' not in row:
                        row['categoryL1_name'] = ''
                    if 'categoryL1_path_code' not in row:
                        row['categoryL1_path_code'] = '0'
                    if 'categoryL2_name' not in row:
                        row['categoryL2_name'] = ''
                    if 'categoryL2_path_code' not in row:
                        row['categoryL2_path_code'] = '0'
                    if 'categoryL3_name' not in row:
                        row['categoryL3_name'] = ''
                    if 'categoryL3_path_code' not in row:
                        row['categoryL3_path_code'] = '0'

                    rows.append(row)
                    if respcount == self.product_search_max_records_to_process:
                        break
            except KeyError as e:
                self.logger.error(f'Error {e} processing product search for {filename}')

        return rows

    def build_request_collection(self, dict_image_to_id, input_url):
        bucket_name = self.config['default']['bucket_name']
        image_path = self.config['default']['image_path']
        if image_path[-1] != '/':
            image_path = image_path + '/'

        requests = []
        dict_img_idx = {}
        index = 0
        for key, value in dict_image_to_id.items():
            source = {"image_uri": f'gs://{bucket_name}/{image_path}{key}'}
            image = {"source": source}
            request = {'image': image,
                       'features': self.features}
            requests.append(request)
            dict_img_idx[index] = (key, value)
            index += 1
        return requests, dict_img_idx

    def submit_batch_annotate_request(self, dict_image_to_id, input_url):
        requests, dict_img_idx = self.build_request_collection(dict_image_to_id, input_url)
        result = []
        chunks = [requests[i * self.batch_size:(i + 1) * self.batch_size] for i in range((len(requests) + self.batch_size - 1) // self.batch_size )]
        self.logger.info(f'submit_batch_annotate_request: num requests:{len(requests)}')
        start_time = time.time()
        bandwidth_remaining = self.max_imgs_per_min
        num_sent_total = 0
        sleep_margin = 2
        response_index = 0
        for chunk in chunks:
            chunk_response = json.loads(MessageToJson(self.vision_client.batch_annotate_images(chunk)))
            bandwidth_remaining -= len(chunk)
            now = time.time()
            delta = now - start_time
            if bandwidth_remaining == 0 and delta < 60:
                to_sleep = 60 - delta + sleep_margin
                self.logger.info(f'Reached bandwidth limit of {self.max_imgs_per_min} in {delta} seconds, sleeping for {to_sleep} seconds')
                time.sleep(to_sleep)
                self.logger.info('Waking up')
                bandwidth_remaining = self.max_imgs_per_min
                start_time = time.time()
            elif delta >= 60:
                bandwidth_remaining = self.max_imgs_per_min
                start_time = time.time()
                self.logger.info(f'New period, resetting remaining bandwidth to {bandwidth_remaining}')

            num_sent_total += len(chunk)
            self.logger.info(f'Requests sent so far: {num_sent_total}')

            for x in range(len(chunk_response['responses'])):
                img_name, img_row = dict_img_idx[response_index]
                response_index += 1
                chunk_response['responses'][x]['img_id'] = img_row
                chunk_response['responses'][x]['img_name'] = img_name

            result.extend(chunk_response['responses'])

        return result

    def batch_annotate(self, dict_image_to_id, input_url = None):
        if not input_url:
            if not self.input_url:
                raise ValueError('input_url must be a valid storage bucket')
            input_url = self.input_url

        try:
            return self.submit_batch_annotate_request(dict_image_to_id, input_url)
        except Exception as e:
            self.logger.error(e)

    def upload_images(self, image_rows):
        dict_image_to_id = {}
        for row in image_rows:
            for asset_name, asset_type in self.image_column_lst:
                if asset_name in row:
                    filename = row[asset_name]
                    if filename:
                        dict_image_to_id[filename] = row['id']
        return dict_image_to_id

    def get_product_catalog_suggestions(self, image_uri):

        # Create the needed clients
        product_search_client = vision_v1.ProductSearchClient()
        image_annotator_client = vision_v1.ImageAnnotatorClient()

        # Create annotate image request along with product search feature.
        image_source = vision_v1.types.ImageSource(image_uri=image_uri)
        image = vision_v1.types.Image(source=image_source)

        # product search specific parameters
        product_set_path = product_search_client.product_set_path(
            project=self.product_search_project_id, 
            location=self.product_search_location, 
            product_set=self.product_search_product_set_id
        )
        product_search_params = vision_v1.types.ProductSearchParams(
            product_set=product_set_path, product_categories=[self.product_search_product_cat], filter=None
        )
        image_context = vision_v1.types.ImageContext(product_search_params=product_search_params)

        # Search products similar to the image.
        response = image_annotator_client.product_search(image, image_context=image_context)

        return MessageToDict(response, preserving_proto_field_name=True)


