import traceback
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, String, DateTime, Boolean, Date, Integer, Numeric, JSON, ARRAY
from sqlalchemy.exc import IntegrityError, OperationalError
from retry import retry
import logging


class db_utils:
    def __init__(self, config, logger, table, columns):
        self.config = config
        self.logger = logger
        self.table = table
        self.columns = columns
        # try:
        #     user = config['database']['postgres_user']
        #     pwd = config['database']['postgres_password']
        #     db = config['database']['postgres_db']
        #     conn = config['database']['postgres_connection']
        # except KeyError as e:
        #     raise SystemExit(f'Error {e} reading configuration')

        db_string = (
                     config['default']['db_string']
                    )
        self.db = create_engine(db_string)

    @staticmethod
    def get_sql_type(text_type):
        return {
            'Integer': Integer,
            'String': String,
            'Numeric': Numeric,
            'Boolean': Boolean,
            'DateTime': DateTime,
            'Date': Date,
            'JSON': JSON,
            'ARRAY(Integer)': ARRAY(Integer),
            'ARRAY(String)': ARRAY(String),
            'ARRAY(Numeric)': ARRAY(Numeric),
            'ARRAY(Boolean)': ARRAY(Boolean),
            'ARRAY(DateTime)': ARRAY(DateTime),
            'ARRAY(Date)': ARRAY(Date),
        }.get(text_type)

    @retry(OperationalError, tries=10, delay=4, backoff=2, logger=logging.getLogger('Sanitizer'))
    def insert(self, rows):        
        Session = sessionmaker(self.db)
        session = Session()
        TableClass = None
        attr_dict = {'__tablename__': self.table}
        returned_rows = []
        for row in rows:
            insert_data = {}
            for name, value in row.items():
                name_l = name.lower()
                if name_l in self.columns:
                    dbval = self.columns[name_l]
                    vals = dbval.split(',')
                    primary = False
                    if len(vals) < 2:
                        session.close()
                        raise ValueError(f'{name} improperly configured')
                    elif len(vals) > 2:
                        primary = vals[2] == 'primary'

                    col_name = vals[0]
                    col_type = self.get_sql_type(vals[1])
                    if not col_type:
                        session.close()
                        raise ValueError(f'{name} defined with unknown type')
                    insert_data[col_name] = value
                    if not TableClass:
                        attr_dict[col_name] = Column(col_type, primary_key=primary)
            
            if insert_data:
                if not TableClass:
                    attr_dict['id'] = Column(Integer, primary_key=True, autoincrement=True, nullable=True)
                    base = declarative_base()
                    TableClass = type(f'TableClass', (base,), attr_dict)
                table_row = TableClass(**insert_data)
                try:
                    session.add(table_row)
                    session.flush()
                    row['id'] = table_row.id
                    returned_rows.append(row)
                    session.commit()
                except IntegrityError as e:
                    self.logger.error(f'Unique constraint violation {e} adding row {row}: {traceback.format_exc()}')
                    session.rollback()
                    session.close()
                    session = Session()

        session.close()
        return returned_rows
