
import os
import sys
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, String, DateTime, Boolean, Date, Integer, Numeric, JSON, ARRAY

class db_utils:
    def __init__(self, config, section='database'):
        self.config = config
        try:
            user = config['database']['postgres_user']
            pwd = config['database']['postgres_password']
            db = config['database']['postgres_db']
            conn = config['database']['postgres_connection']
        except KeyError as e:
            raise SystemExit(f'Error {e} reading configuration')

        db_string = (
                     f'postgresql+psycopg2://{user}:{pwd}@/{db}?host=/cloudsql/{conn}'
                    )
        self.db = create_engine(db_string)

    @staticmethod
    def get_sql_type(text_type):
        return {
            'Integer': Integer,
            'String': String,
            'Numeric': Numeric,
            'Boolean': Boolean,
            'DateTime': DateTime,
            'Date': Date,
            'JSON': JSON,
            'ARRAY(Integer)': ARRAY(Integer),
            'ARRAY(String)': ARRAY(String),
            'ARRAY(Numeric)': ARRAY(Numeric),
            'ARRAY(Boolean)': ARRAY(Boolean),
            'ARRAY(DateTime)': ARRAY(DateTime),
            'ARRAY(Date)': ARRAY(Date),
        }.get(text_type)

    def insert(self, rows, section):
        try:
            schema_section = self.config[section]['schema_section']
            table_name = self.config[section]['table']
        except KeyError as e:
            raise SystemExit(f'Error {e} reading configuration')
        
        Session = sessionmaker(self.db)
        session = Session()
        attr_dict = {'__tablename__': table_name}
        for row in rows:
            insert_data = {}
            for name, value in row.items():
                if name in self.config[section]:
                    vals = value.split(',')
                    if len(vals) != 2:
                        session.close()
                        raise ValueError(f'{name} improperly configured')

                    col_name = vals[0]
                    col_type = self.get_sql_type(vals[1])
                    if not col_type:
                        session.close()
                        raise ValueError(f'{name} defined with unknown type')
                    insert_data[col_name] = value
                    attr_dict[col_name] = col_type
            
            if insert_data:
                base = declarative_base()
                TableClass = type('TableClass', (base,), attr_dict)
                row = TableClass(**insert_data)
                try:
                    session.add(row)
                    session.commit()
                except Exception:
                    logger.error(f'postgres_write error adding row {row}: {traceback.format_exc()}')
                    session.rollback()
                    session.close()
                    raise RuntimeError(f'Failed to insert row')

        session.close()
        