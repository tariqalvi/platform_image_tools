
import os
import sys
import json
import time
from google.protobuf.json_format import MessageToJson
from google.cloud import vision_v1
from google.cloud.vision_v1 import enums
from google.cloud import storage

os.environ['GOOGLE_PROJECT_ID'] = 'tariq-alvi-sb'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/xtxa086/Downloads/tariq-alvi-sb-04d810e3789e.json'

class image_utils:
    def __init__(self, config):
        self.input_url = None
        if 'input_bucket' in config['annotation']:
            self.input_url = config['annotation']['input_bucket']
        
        self.batch_size = 16
        if 'batch_size' in config['annotation']:
            self.batch_size = min(self.batch_size, int(config['annotation']['batch_size']))

        self.max_imgs_per_min = 1800
        if 'max_imgs_per_min' in config['annotation']:
            self.max_imgs_per_min = min(self.max_imgs_per_min, int(config['annotation']['max_imgs_per_min']))

        self.features = [{"type": enums.Feature.Type.LABEL_DETECTION},]
                         # {"type": enums.Feature.Type.OBJECT_LOCALIZATION},
                         # {"type": enums.Feature.Type.WEB_DETECTION},
                         # {"type": enums.Feature.Type.PRODUCT_SEARCH},
                         #{"type": enums.Feature.Type.SAFE_SEARCH_DETECTION},]
        self.vision_client = vision_v1.ImageAnnotatorClient()


    def build_request_collection(self, input_url):
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(input_url)
        blobs = bucket.list_blobs()
        requests = []
        for blob in blobs:
            source = {"image_uri": f'gs://{input_url}/{blob.name}'}
            image = {"source": source}

            request = {'image' : image,
                       'features' : self.features}
            requests.append(request)
        return requests

    def submit_batch_annotate_request(self, input_url):
        requests = self.build_request_collection(input_url)
        result = []
        chunks = [requests[i * self.batch_size:(i + 1) * self.batch_size] for i in range((len(requests) + self.batch_size - 1) // self.batch_size )]
        start_time = time.time()
        num_sent_this_minute = 0
        for chunk in chunks:
            if num_sent_this_minute + len(chunk) >= self.max_imgs_per_min:
                now = time.time()
                time_delta = now - start_time
                if time_delta < 60:
                    time.sleep(60-time_delta)
                num_sent_this_minute = 0
                start_time = now
                
            chunk_response = json.loads(MessageToJson(self.vision_client.batch_annotate_images(chunk)))
            result.extend(chunk_response['responses'])

        return result

    def batch_annotate(self, input_url = None):
        if not input_url:
            if not self.input_url:
                raise ValueError('input_url must be a valid storage bucket')
            input_url = self.input_url

        # start = time.time()
        try:
            return self.submit_batch_annotate_request(input_url)
        except Exception as e:
            print(e)
