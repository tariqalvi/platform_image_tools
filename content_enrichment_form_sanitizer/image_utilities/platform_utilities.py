
import os
import sys
import json
import time

class platform_utils:
    def __init__(self, config):
        self.config = config
        try:
            os.environ['GOOGLE_PROJECT_ID'] = config['credentials']['GOOGLE_PROJECT_ID']
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config['credentials']['GOOGLE_APPLICATION_CREDENTIALS']
            self.cef_columns = list(config.items('cef_section'))
            self.image_columns = list(config.items('image_section'))
        except KeyError as e:
            raise SystemExit(f'Error {e} reading configuration')


    def process_non_image_data(excel_rows):
        bq_data = []
        for excel_row in excel_rows:
            db_row = []
            for column in self.cef_columns:
                db_row.append(excel_row[column])
            bq_data.append(tuple(db_row))

    def process_content_enrichment(excel_rows):
        process_non_image_data(excel_rows)
        




