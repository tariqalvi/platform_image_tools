import os
import re
import shutil
import logging
import traceback
import configparser
import requests
from requests.exceptions import HTTPError, ConnectionError, Timeout, RequestException
from urllib.parse import urlparse
from openpyxl import load_workbook
from google.cloud import storage
from google.cloud.exceptions import NotFound, ServiceUnavailable, GatewayTimeout, InternalServerError, TooManyRequests
from retry import retry
import logging
from platform_utilities import platform_utils

image_names_dic = {}

storage_client = None

def storage_Client():
    global storage_client
    if not storage_client:
        storage_client = storage.Client()
    return storage_client

@retry((ServiceUnavailable, GatewayTimeout, InternalServerError, TooManyRequests), tries=10, delay=4, backoff=2, logger=logging.getLogger('Sanitizer'))
def move_file_to_bucket(supplier, filename, folder):
    client = storage_Client()
    bucket = client.get_bucket(os.environ['BUCKET_NAME'])
    blob = bucket.blob(f'{supplier}/{folder}/{filename}')
    blob.upload_from_filename(filename)


class Sanitizer:
    def __init__(self,
                 form_name,
                 stock_no_column_name,
                 input_file,
                 output_file,
                 non_img_repl_regex,
                 img_repl_regex,
                 replace_with,
                 ignored_col_list,
                 supported_extensions_list,
                 scrape_sites,
                 scrape_expressions,
                 config):

        self.logger = logging.getLogger('Sanitizer')
        self.work_book = None
        self.work_sheet = None
        self.missing_imgs_work_sheet = None
        self.form_name = form_name
        self.stock_no_column_name = stock_no_column_name
        self.input_file = input_file
        self.output_file = output_file
        self.non_img_repl_regex = non_img_repl_regex
        self.img_repl_regex = img_repl_regex
        self.replace_with = replace_with
        self.ignored_col_list = ignored_col_list
        self.supported_extensions_list = supported_extensions_list
        self.scrape_sites = scrape_sites
        self.scrape_expressions = scrape_expressions
        self.config = config
        self.row_data = {}
        self.missing_images_data = []
        self.missing_images_set = set()

    def terminate(self):
        if self.work_book:
            self.work_book.save(self.output_file)

    def create_work_file(self):
        error_string = ''
        try:
            shutil.copyfile(self.input_file, self.output_file)
            self.logger.info(f'{self.input_file}->{self.output_file}')
        except shutil.SameFileError:
            error_string = 'Source and destination files are the same.'
        except IsADirectoryError:
            error_string = 'Destination is a directory.'
        except PermissionError:
            error_string = 'Permission denied creating work file.'
        except Exception as e:
            error_string = f'Error occurred while copying file. {e}'

        if error_string:
            self.logger.error(error_string)
            raise SystemExit(error_string)

    def get_worksheet(self):
        self.create_work_file()
        self.work_book = load_workbook(self.output_file)
        sheets = self.work_book.sheetnames
        if self.form_name not in sheets:
            self.logger.error(f'{self.form_name} not present.')
            raise SystemExit(f'{self.form_name} not present.')

        self.missing_imgs_work_sheet = self.work_book.create_sheet('Missing images')
        return self.work_book[f'{self.form_name}']

    def add_row_data(self, row_index, column_name, value):
        if row_index not in self.row_data:
            self.row_data[row_index] = {'asset_count': 0}

        self.row_data[row_index][column_name] = value

    @staticmethod
    def is_url(url):
        try:
            result = urlparse(url)
            path = result.path
            tokens = path.split('/')
            img_file = tokens[len(tokens) - 1]
            is_url = True
            if not result.hostname:
                is_url = False

            return is_url, img_file
        except ValueError:
            return False, None

    @staticmethod
    @retry((HTTPError, ConnectionError, Timeout, RequestException), tries=10, delay=4, backoff=2, logger=logging.getLogger('Sanitizer'))
    def download_img(config, url, file_name):
        r = requests.get(url, timeout=10)
        if r.status_code != 200:
            return False
        image = r.content
        if not image:
            return False

        client = storage_Client()
        try:
            bucket = client.get_bucket(config['default']['bucket_name'])
            blob = bucket.blob(f'{config["default"]["image_path"]}{file_name}')
            blob.upload_from_string(image)
            image_names_dic[file_name.lower()] = file_name
        except NotFound:
            raise SystemExit(f'Failed to upload {url}!')
        except Exception as e:
            return False

        return True

    def asset_exists(self, asset_name):
        return image_names_dic.get(asset_name.lower())

    @retry(Exception, tries=10, delay=4, backoff=2, logger=logging.getLogger('Sanitizer'))
    def rename_asset(self, initial, final):
        real_asset_name = image_names_dic[initial.lower()]
        bucket_name = self.config['default']['bucket_name']
        image_path = self.config['default']['image_path']

        storage_client = storage_Client()
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(f'{image_path}{real_asset_name}')
        bucket.rename_blob(blob, f'{image_path}{final}')
        image_names_dic[initial.lower()] = final

    def process_worksheet(self):
        ws = self.get_worksheet()
        self.missing_imgs_work_sheet['A1'].value = 'Asset'
        self.missing_imgs_work_sheet['B1'].value = 'Supplier Stock no'
        missing_ws_index = 2
        for col_index, col in enumerate(ws.iter_cols()):
            column_name = ws.cell(2, col_index + 1).value
            if not column_name:
                continue

            is_asset_col = True if column_name.startswith('Asset Name') else False

            num_rows_processed = 0
            for row_index, cell in enumerate(col):
                num_rows_processed += 1
                if not ws.cell(row_index+1, 1).value:
                    break

                if num_rows_processed % 10 == 0:
                    self.logger.info(f'Processing row {num_rows_processed} in column {column_name}')

                value = cell.value
                self.add_row_data(row_index, column_name, value)

                if value and column_name not in self.ignored_col_list and row_index > 1:
                    if is_asset_col:
                        is_url, img_file = Sanitizer.is_url(value)
                        if is_url:
                            if not self.asset_exists(img_file):
                                Sanitizer.download_img(self.config, value, img_file)
                            value = img_file

                        stock_no = ''
                        if self.stock_no_column_name in self.row_data[row_index]:
                            stock_no = self.row_data[row_index][self.stock_no_column_name]

                        actual_name = self.asset_exists(value)
                        if actual_name:
                            _, extension = os.path.splitext(actual_name)
                            if extension.lower() not in self.supported_extensions_list:
                                self.logger.warning(
                                    f'Image {value} has invalid extension! [Row {row_index + 1} stock no {stock_no}]')
                                actual_name = None

                        if not actual_name:
                            # self.logger.warning(
                            #     f'Image {value} not found! [Row {row_index + 1} stock no {stock_no}]')
                            value_sanitized = None
                            ws.cell(cell.row, cell.column+1).value = ''
                            self.missing_imgs_work_sheet[f'A{missing_ws_index}'].value = value
                            self.missing_imgs_work_sheet[f'B{missing_ws_index}'].value = stock_no
                            missing_image_row = {'supplier_short_name': self.config['default']['supplier'],
                                                 'supplier_stock_num': stock_no,
                                                 'asset':  value,
                                                 'scrape_sites': self.scrape_sites,
                                                 'scrape_allow_expressions': self.scrape_expressions
                                                 }
                            if value not in self.missing_images_set:
                                self.missing_images_set.add(value)
                                self.missing_images_data.append(missing_image_row)
                            missing_ws_index += 1
                        else:
                            value = actual_name

                            value_sanitized = re.sub(self.img_repl_regex, self.replace_with, value)

                            if value != value_sanitized:
                                self.rename_asset(value,
                                                  value_sanitized)

                            self.row_data[row_index]['asset_count'] += 1
                    else:
                        value_sanitized = value

                    cell.value = value_sanitized

        self.logger.info('Cleaning up rows with no assets')

        # Work backwards so you dont have to adjust row indexes because of deleted rows
        # for idx in reversed(range(row_index+1)):
        #     if idx-1 not in self.row_data:
        #         break
        #
        #     if self.row_data[idx-1]['asset_count'] == 0 and idx > 2:
        #         ws.delete_rows(idx)
        try:
            for idx in reversed(range(row_index+1)):
                #if idx not in self.row_data.keys():
                #    break
                if idx > 2 and self.row_data[idx-1]['asset_count'] == 0:
                    ws.delete_rows(idx)
        except Exception as e:
            logger = logging.getLogger('Sanitizer')
            logger.info(f'Error {e} while checking for rows with no assets {traceback.format_exc()}')

        self.logger.info('Cef form sanitized, copying to output')

    def perform_etl(self, config):
        self.work_book = load_workbook(self.output_file)
        ws = self.work_book[f'{self.form_name}']
        columns = []
        max_col_index = 0
        for col_index, col in enumerate(ws.iter_cols()):
            col_hdr = ws.cell(2, col_index + 1).value
            if not col_hdr:
                break
            columns.append(col_hdr)
            max_col_index = col_index

        data = []
        for row_index, row in enumerate(ws.iter_rows()):
            if row_index > 1:
                col1_val = ws.cell(row_index+1, 1).value
                if not col1_val:
                    break
                row_data = {}
                for col_index in range(max_col_index):
                    row_data[columns[col_index]] = ws.cell(row_index+1, col_index+1).value
                data.append(row_data)

        try:
            pu = platform_utils(config, self.logger, image_names_dic.keys())
            pu.process_content_enrichment(data, self.missing_images_data)
        except Exception as e:
            self.logger.info(f'Error {e} in content enrichment')

        self.logger.info('Processing completed!')
        move_file_to_bucket(self.config['default']['supplier'],
                            self.config['default']['log_file'],
                            'Logs')


def sanitize(form_name,
             stock_no_column_name,
             input_file,
             output_file,
             non_img_repl_regex,
             img_repl_regex,
             replace_with,
             ignored_col_list,
             supported_extensions_list,
             scrape_sites,
             scrape_expressions,
             config):
    s = Sanitizer(form_name,
                  stock_no_column_name,
                  input_file,
                  output_file,
                  non_img_repl_regex,
                  img_repl_regex,
                  replace_with,
                  ignored_col_list,
                  supported_extensions_list,
                  scrape_sites,
                  scrape_expressions,
                  config)

    s.process_worksheet()
    s.terminate()
    move_file_to_bucket(config['default']['supplier'],
                        output_file, 'Cef_output')
    logger = logging.getLogger('Sanitizer')
    logger.info('Processing completed!')
    move_file_to_bucket(config['default']['supplier'],
                        config['default']['log_file'],
                        'Logs')
    #s.perform_etl(config)


def create_logger(log_file):
    logger = logging.getLogger('Sanitizer')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger


def get_config():
    if not os.path.exists('config.ini'):
        raise SystemExit(f'Global config is missing')

    config_global = configparser.ConfigParser()
    config_global.read('config.ini')

    try:
        supplier_name = config_global['default']['cef_current']
        cef_file_name = config_global['default']['cef_file']
    except KeyError:
        raise SystemExit(f'Failed to read configuration from global config: {e}')

    out_file, extension = os.path.splitext(cef_file_name)

    logfile = f'{supplier_name}_{out_file}.log'
    logger = create_logger(logfile)

    logger.info(f'Using cef_current:{supplier_name} cef_file:{cef_file_name}')

    unattended = False
    try:
        unattended = os.environ['UNATTENDED']
        if unattended.lower() == 'true':
            unattended = True
    except KeyError:
        pass

    if not unattended:
        answer = input('Is the above info correct? (y/n)')
        if answer == 'n' or answer == 'N':
            raise SystemExit('Please correct the configuration and restart')

    try:
        bucket_name = os.environ['BUCKET_NAME']
        storage_client = storage_Client()
        bucket = storage_client.get_bucket(bucket_name)
    except Exception as e:
        raise SystemExit(f'Failed to locate GCP bucket: {e}')

    try:
        blob = bucket.blob(f'{supplier_name}/Config/config_local.ini')
        blob.download_to_filename('config_local.ini')
    except Exception as e:
        raise SystemExit(f'Failed to download config_local.ini: {e}')

    try:
        blob = bucket.blob(f'{supplier_name}/Cef_input/{cef_file_name}')
        blob.download_to_filename('in.xlsx')
    except Exception as e:
        raise SystemExit(f'Failed to download {cef_file_name}: {e}')

    image_path = f'{supplier_name}/Images/{out_file}/'

    img_path_exists = False
    for blob in storage_client.list_blobs(bucket_name, prefix=image_path):
        img_path_exists = True
        break

    if not img_path_exists:
        raise SystemExit(f'Image location {image_path} does not exist!')

    config_local = configparser.ConfigParser()
    config_local.read('config_local.ini')

    config_local.set('default', 'supplier', supplier_name)
    config_local.set('default', 'bucket_name', bucket_name)
    config_local.set('default', 'output_file_name', f'{out_file}_out.xlsx')
    config_local.set('default', 'image_path', image_path)
    config_local.set('default', 'log_file', logfile)
    return config_local, logger


def initialize_image_name_set(config):
    bucket_name = config['default']['bucket_name']

    try:
        storage_client = storage_Client()
        blobs = storage_client.list_blobs(bucket_name, prefix=config['default']['image_path'])
        for blob in blobs:
            name_to_add = blob.name[len(config['default']['image_path']):]
            if name_to_add:
                image_names_dic[name_to_add.lower()] = name_to_add
    except Exception as e:
        raise SystemExit('Failed to initialize image names set')


def main():
    config, logger = get_config()
    initialize_image_name_set(config)

    try:
        supported_extensions = config['default']['supported_file_extensions']
        scrape_sites = config['default']['scrape_sites']
        scrape_expressions = config['default']['scrape_expressions']
        form_name = config['default']['form_name'].strip()
        stock_no_column_name = config['default']['stock_number_column_name'].strip()
        input_file = 'in.xlsx'
        output_file = config['default']['output_file_name']
        non_img_repl_regex = config['default']['non_img_repl_regex'].strip()
        img_repl_regex = config['default']['img_repl_regex'].strip()
        replace_with = config['default']['replace_with'].strip()
        ignored_columns = config['default']['ignored_columns'].strip()
    except KeyError as e:
        raise SystemExit(f'Error {e} reading configuration')

    ignored_col_list = ignored_columns.split(',')
    supported_extensions_list = supported_extensions.split(',')

    logger.info('********************Starting sanitize session')

    try:
        sanitize(form_name,
                 stock_no_column_name,
                 input_file,
                 output_file,
                 non_img_repl_regex,
                 img_repl_regex,
                 replace_with,
                 ignored_col_list,
                 supported_extensions_list,
                 scrape_sites,
                 scrape_expressions,
                 config)
    except Exception as e:
        logger = logging.getLogger('Sanitizer')
        logger.info(f'Error {e} in process_worksheet {traceback.format_exc()}')


if __name__ == "__main__":
    main()
