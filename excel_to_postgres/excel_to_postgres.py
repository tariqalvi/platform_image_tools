import sys
from retry import retry
import configparser
from openpyxl import load_workbook
import logging
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, String, DateTime, Boolean, Date, Integer, Numeric, JSON, ARRAY
from sqlalchemy.exc import IntegrityError, OperationalError


def create_logger():
    logger = logging.getLogger('excel_to_postgres')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler('excel_to_postgres')
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger

def get_sql_type(text_type):
    return {
        'Integer': Integer,
        'String': String,
        'Numeric': Numeric,
        'Boolean': Boolean,
        'DateTime': DateTime,
        'Date': Date,
        'JSON': JSON,
        'ARRAY(Integer)': ARRAY(Integer),
        'ARRAY(String)': ARRAY(String),
        'ARRAY(Numeric)': ARRAY(Numeric),
        'ARRAY(Boolean)': ARRAY(Boolean),
        'ARRAY(DateTime)': ARRAY(DateTime),
        'ARRAY(Date)': ARRAY(Date),
    }.get(text_type)

def get_excel_data(excel_file, tab_name, start_row, stop_on_empty_row, max_rows, schema_mapping):
    work_book = load_workbook(excel_file)
    ws = work_book[tab_name]

    data = []
    column_names = []
    columns = {}
    num_rows = 0
    for row_index, row in enumerate(ws.iter_rows()):
        if row_index < start_row:
            continue

        num_rows += 1
        if row_index == start_row:
            done = False
            col_index = 1
            while not done:
                cell_value = ws.cell(row_index, col_index).value
                if not cell_value:
                    done = True
                else:
                    rhs = schema_mapping.get(cell_value.lower())
                    if rhs:
                        columns[col_index] = rhs
                col_index += 1
        else:
            row = {}
            for key, value in columns.items():
                cell_value = ws.cell(row_index, key).value
                if cell_value:
                    row[value] = cell_value

            if stop_on_empty_row:
                if row:
                    data.append(row)
                else:
                    break
            else:
                data.append(row)

        if max_rows and num_rows == max_rows:
            break

    return data


def get_schema_mapping(config):
    try:
        schema_entries = dict(config.items('schema'))
    except KeyError:
        SystemExit('Schema not defined')

    schema_mapping = {}
    attr_dict = {}
    for name, value in schema_entries.items():
        vals = value.split(',')
        primary = False
        nullable = True
        if len(vals) < 2:
            raise ValueError(f'Schema entry {value} improperly formatted')
        elif len(vals) > 2:
            primary = vals[2] == 'primary'
            if len(vals) > 3:
                nullable = vals[3] != 'non-nullable'

        col_name = vals[0]
        col_type = get_sql_type(vals[1])
        if not col_type:
            raise ValueError(f'Unknown type for schema entry {value}')
        schema_mapping[name] = col_name
        attr_dict[col_name] = Column(col_type, primary_key=primary, nullable=nullable)

    return schema_mapping, attr_dict

@retry(OperationalError, tries=10, delay=4, backoff=2, logger=logging.getLogger('excel_to_postgres'))
def insert_db(db, table, attr_dict, rows):
    logger = logging.getLogger('excel_to_postgres')
    logger.info(f'Upserting {len(rows)} records')
    Session = sessionmaker(db)
    session = Session()
    attr_dict['__tablename__'] = table
    base = declarative_base()
    TableClass = type('TableClass', (base,), attr_dict)
    for row in rows:
        table_row = TableClass(**row)
        try:
            session.merge(table_row)
            session.flush()
            session.commit()
        except IntegrityError as e:
            logger.error(f'Unique constraint violation adding row {row} to table {table}')
            session.rollback()
            session.close()
            session = Session()

    session.close()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        SystemExit('No config supplied')

    config_file = sys.argv[1]
    config = configparser.ConfigParser()
    config.read(config_file)

    try:
        excel_file = config['default']['excel_file']
        tab_name = config['default']['tab_name']
        start_row = int(config['default']['start_row'])
        table_name = config['default']['table_name']
        db_string = config['default']['db_string']
    except KeyError:
        SystemExit('Invalid config')

    stop_on_empty_row = False
    max_rows = 0

    if 'stop_on_empty_row' in config['default']:
        stop_on_empty_row = bool(config['default']['stop_on_empty_row'])

    if 'max_rows' in config['default']:
        max_rows = int(config['default']['max_rows'])
        if max_rows < 1:
            SystemExit('max_rows must be at least 1(including column header')

    if not stop_on_empty_row and max_rows == 0:
        SystemExit('Either stop_on_empty_row or max_rows must be configured')
    elif stop_on_empty_row and max_rows > 0:
        SystemExit('Only one of stop_on_empty_row or max_rows must be configured')

    create_logger()
    schema_mapping, attr_dict = get_schema_mapping(config)
    data = get_excel_data(excel_file, tab_name, start_row, stop_on_empty_row, max_rows, schema_mapping)
    db = create_engine(db_string)
    insert_db(db, table_name, attr_dict, data)

