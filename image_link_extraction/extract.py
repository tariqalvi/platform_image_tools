import configparser
import logging
import os
import sys
import time

import requests
from google.cloud import storage
from google.cloud.exceptions import NotFound
from openpyxl import load_workbook


class Store:
    def __init__(self, cfg, logger):
        self.cfg = cfg
        self.logger = logger

        self.bucket = None
        if self.cfg['bucket_name']:
            client = storage.Client()
            try:
                self.bucket = client.get_bucket(self.cfg['bucket_name'])
            except NotFound:
                if self.cfg['create_bucket'] is True:
                    self.bucket = client.create_bucket(self.cfg['bucket_name'])
                else:
                    raise SystemExit(f'Bucket {self.cfg["bucket_name"]} does not exist!')
            except Exception as e:
                raise SystemExit(f'Error {e} getting bucket {self.cfg["bucket_name"]}!')

        if self.cfg['image_path']:
            if not os.path.exists(self.cfg['image_path']):
                if self.cfg['create_path'] is True:
                    os.mkdir(self.cfg['image_path'])
                else:
                    raise SystemExit(f'Folder {self.cfg["image_path"]} does not exist!')

    def store_in_bucket(self, img_file, image):
        blob = self.bucket.blob(img_file)
        blob.upload_from_string(image)

    def store_on_disk(self, img_file, image):
        with open(f'{self.cfg["image_path"]}{img_file}', 'wb') as f:
            f.write(image)

    def store(self, img_file, image):
        if self.bucket:
            self.store_in_bucket(img_file, image)

        if self.cfg['image_path']:
            self.store_on_disk(img_file, image)


class ImageProcessor:
    def __init__(self, cfg, logger):
        self.cfg = cfg
        self.logger = logger
        self.work_book = load_workbook(self.cfg['workbook'])
        self.work_sheet = self.work_book.active
        self.store = Store(cfg, logger)
        self.num_entries_processed = 0
        self.num_stored = 0
        self.num_failed = 0

    def download_image(self, url):
        r = requests.get(url)
        if r.status_code != 200:
            self.logger.error(f'Failed to download {url}, status code {r.status_code}')
            self.num_failed += 1
            return None
        return r.content

    def process(self):
        start_time = time.time()
        done = False
        for idx, cell in enumerate(self.work_sheet[self.cfg['image_column']]):
            if idx > 0:
                self.num_entries_processed += 1
                val = cell.value
                images = val.split('\n')
                for image_link in images:
                    image = self.download_image(image_link)
                    if image:
                        tokens = image_link.split('/')
                        img_file = tokens[len(tokens) - 1]
                        self.store.store(img_file, image)
                        self.num_stored += 1
                        delta = time.time() - start_time

                        if self.cfg['log_every'] > 0 and self.num_stored % self.cfg['log_every'] == 0:
                            self.logger.info(f'Stored {self.num_stored}')

                        if self.num_stored == self.cfg['download_image_limit']:
                            self.logger.info(f'Stopping due to max image limit of {self.cfg["download_image_limit"]}')
                            done = True
                            break

                        if delta >= self.cfg['download_time_limit']:
                            self.logger.info(
                                f'Stopping due to max time limit of {self.cfg["download_time_limit"]} seconds')
                            done = True
                            break

            if done is True:
                break

        self.logger.info(
            f'Completed - {self.num_stored} images from {self.num_entries_processed} rows in {format(time.time() - start_time, ".2f")} seconds, with {self.num_failed} failures.')


def get_configuration(config_file):
    if not os.path.exists(config_file):
        raise SystemExit(f'Config file {config_file} not found!')

    config = configparser.ConfigParser()
    config.read(config_file)
    cfg_dict = {}
    try:
        cfg_dict['workbook'] = config['default']['workbook'].strip()
        cfg_dict['log_file'] = config['default']['log_file'].strip()
        cfg_dict['image_column'] = config['default']['image_column'].strip()
    except KeyError as e:
        raise SystemExit(f'Error {e} reading configuration')

    cfg_dict['bucket_name'] = None
    if 'bucket_name' in config['default']:
        cfg_dict['bucket_name'] = config['default']['bucket_name'].strip()

    cfg_dict['image_path'] = None
    if 'image_path' in config['default']:
        cfg_dict['image_path'] = config['default']['image_path'].strip()

    cfg_dict['create_bucket'] = True
    if 'create_bucket' in config['default']:
        cfg_dict['create_bucket'] = bool(config['default']['create_bucket'].strip())

    cfg_dict['create_path'] = True
    if 'create_path' in config['default']:
        cfg_dict['create_path'] = bool(config['default']['create_path'].strip())

    cfg_dict['log_every'] = 10
    if 'log_every' in config['default']:
        cfg_dict['log_every'] = int(config['default']['log_every'].strip())

    cfg_dict['download_image_limit'] = 10000
    if 'download_image_limit' in config['default']:
        cfg_dict['download_image_limit'] = int(config['default']['download_image_limit'].strip())

    cfg_dict['download_time_limit'] = 24 * 60 * 60
    if 'download_time_limit' in config['default']:
        cfg_dict['download_time_limit'] = int(config['default']['download_time_limit'].strip())

    return cfg_dict


def create_logger(cfg):
    logger = logging.getLogger('Extract')
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler(cfg['log_file'])
    fh.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger


def main(cfg):
    logger = create_logger(cfg)
    image_processor = ImageProcessor(cfg, logger)
    image_processor.process()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise SystemExit('Incorrect arguments!')

    cfg = get_configuration(sys.argv[1])
    main(cfg)
