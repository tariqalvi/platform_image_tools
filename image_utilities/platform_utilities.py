
import os
import sys
import json
import time
from db_utilities import db_utils
from image_utilities import image_utils

class platform_utils:
    def __init__(self, config):
        self.config = config
        try:
            os.environ['GOOGLE_PROJECT_ID'] = config['credentials']['GOOGLE_PROJECT_ID']
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config['credentials']['GOOGLE_APPLICATION_CREDENTIALS']
            self.cef_table = config[config['default']['cef_db_section']]['table']
            self.image_table = config[config['default']['image_db_section']]['table']
            self.label_table = config[config['default']['label_db_section']]['table']
            self.ss_table = config[config['default']['ss_db_section']]['table']
            self.web_best_guess_label_table = config[config['default']['wbgl_db_section']]['table']
            self.web_entities_table = config[config['default']['web_entities_db_section']]['table']
            self.web_fmi_table = config[config['default']['web_fmi_db_section']]['table']
            self.web_pmi_table = config[config['default']['web_pmi_db_section']]['table']
            self.web_pwmi_table = config[config['default']['web_pwmi_db_section']]['table']
            self.web_vsi_table = config[config['default']['web_vsi_db_section']]['table']
            self.product_search_table = config[config['default']['product_search_db_section']]['table']
            self.cef_columns = dict(config.items(config[config['default']['cef_db_section']]['schema_section']))
            self.image_columns = dict(config.items(config[config['default']['image_db_section']]['schema_section']))
            self.label_columns = dict(config.items(config[config['default']['label_db_section']]['schema_section']))
            self.ss_columns = dict(config.items(config[config['default']['ss_db_section']]['schema_section']))
            self.web_best_guess_label_columns = dict(config.items(config[config['default']['wbgl_db_section']]['schema_section']))
            self.web_entities_columns = dict(config.items(config[config['default']['web_entities_db_section']]['schema_section']))
            self.web_fmi_columns = dict(config.items(config[config['default']['web_fmi_db_section']]['schema_section']))
            self.web_pmi_columns = dict(config.items(config[config['default']['web_pmi_db_section']]['schema_section']))
            self.web_pwmi_columns = dict(config.items(config[config['default']['web_pwmi_db_section']]['schema_section']))
            self.web_vsi_columns = dict(config.items(config[config['default']['web_vsi_db_section']]['schema_section']))
            self.product_search_columns = dict(config.items(config[config['default']['product_search_db_section']]['schema_section']))
            image_column_count = int(config['default']['image_column_count'])
            image_column_pairs = config['default']['image_column_pairs']
            image_column_pair_val = image_column_pairs.split(',')
            self.image_column_lst = []
            for x in range(1, image_column_count+1):
                self.image_column_lst.append((f'{image_column_pair_val[0]} {x}', f'{image_column_pair_val[1]} {x}')) 
        except KeyError as e:
            raise SystemExit(f'Error {e} reading configuration')


    def process_non_image_data(self, excel_rows):
        du = db_utils(self.config, self.cef_table, self.cef_columns)
        return du.insert(excel_rows)

    def process_image_data(self, cef_rows):
        image_rows = []
        for row in cef_rows:
            for asset_name, asset_type in self.image_column_lst:
                if asset_name in row and asset_type in row:
                    row_data = {'cef_id' : row['id']}
                    row_data[asset_name] = row[asset_name]
                    row_data[asset_type] = row[asset_type]
                    image_rows.append(row_data)

        du = db_utils(self.config, self.image_table, self.image_columns)
        return du.insert(image_rows)

    @staticmethod
    def init_row(columns, result):
        row = {}
        for column in columns:
            row[column] = None
        row['images_id'] = result['img_id']
        return row

    def process_annotations(self, results):
        label_rows = []
        web_best_guess_label_rows = []
        web_entities_rows = []
        web_fmi_rows = []
        web_pmi_rows = []
        web_pwmi_rows = []
        web_vsi_rows = []
        ss_rows = []
        for result in results:
            if 'labelAnnotations' in result:
                for annotation in result['labelAnnotations']:
                    label_row = self.init_row(self.label_columns, result)
                    for key, value in annotation.items():
                        label_row[key.lower()] = value
                    label_rows.append(label_row)

            if 'safeSearchAnnotation' in result:
                ss_row = self.init_row(self.ss_columns, result)
                for key, value in result['safeSearchAnnotation'].items():
                    ss_row[key.lower()] = value
                ss_rows.append(ss_row)
            
            if 'webDetection' in result:
                if 'bestGuessLabels' in result['webDetection']:
                    for label in result['webDetection']['bestGuessLabels']:
                        web_best_guess_label_row = self.init_row(self.web_best_guess_label_columns, result)
                        for key, value in label.items():
                            web_best_guess_label_row[key.lower()] = value
                        web_best_guess_label_rows.append(web_best_guess_label_row)
                    
                if 'webEntities' in result['webDetection']:
                    for entity in result['webDetection']['webEntities']:
                        web_entities_row = self.init_row(self.web_entities_columns, result)
                        for key, value in entity.items():
                            web_entities_row[key.lower()] = value
                        web_entities_rows.append(web_entities_row)

                if 'fullMatchingImages' in result['webDetection']:
                    for fmi in result['webDetection']['fullMatchingImages']:
                        web_fmi_row = self.init_row(self.web_fmi_columns, result)
                        for key, value in fmi.items():
                            web_fmi_row[key.lower()] = value
                        web_fmi_rows.append(web_fmi_row)

                if 'partialMatchingImages' in result['webDetection']:
                    for pmi in result['webDetection']['partialMatchingImages']:
                        web_pmi_row = self.init_row(self.web_pmi_columns, result)
                        for key, value in pmi.items():
                            web_pmi_row[key.lower()] = value
                        web_pmi_rows.append(web_pmi_row)
                
                if 'pagesWithMatchingImages' in result['webDetection']:
                    for pwmi in result['webDetection']['pagesWithMatchingImages']:
                        if 'partialMatchingImages' in pwmi:
                            for urldict in pwmi['partialMatchingImages']:
                                if 'url' in urldict:
                                    web_pwmi_row = self.init_row(self.web_pwmi_columns, result)
                                    web_pwmi_row['partialmatchingimages'] = urldict['url']           
                                    if 'pageTitle' in pwmi:
                                        web_pwmi_row['pagetitle'] = pwmi['pageTitle']
                                    if 'url' in pwmi:
                                        web_pwmi_row['url'] = pwmi['url']
                                    web_pwmi_rows.append(web_pwmi_row)

                if 'visuallySimilarImages' in result['webDetection']:
                    for vsi in result['webDetection']['visuallySimilarImages']:
                        if 'url' in vsi:
                            web_vsi_row = self.init_row(self.web_vsi_columns, result)
                            web_vsi_row['url'] = vsi['url']           
                            web_vsi_rows.append(web_vsi_row)

        print("Adding annotation data to db")
        if len(label_rows) > 0:
            print("Labels")
            du = db_utils(self.config, self.label_table, self.label_columns)
            du.insert(label_rows)
        
        if len(ss_rows) > 0:
            print("Safe search")
            du = db_utils(self.config, self.ss_table, self.ss_columns)
            du.insert(ss_rows)

        if len(web_best_guess_label_rows) > 0:
            print("Best guess")
            du = db_utils(self.config, self.web_best_guess_label_table, self.web_best_guess_label_columns)
            du.insert(web_best_guess_label_rows)
        
        if len(web_entities_rows) > 0:
            print("Web entities")
            du = db_utils(self.config, self.web_entities_table, self.web_entities_columns)
            du.insert(web_entities_rows)

        if len(web_fmi_rows) > 0:
            print("Fully matching images")
            du = db_utils(self.config, self.web_fmi_table, self.web_fmi_columns)
            du.insert(web_fmi_rows)

        if len(web_pmi_rows) > 0:
            print("Partially matching images")
            du = db_utils(self.config, self.web_pmi_table, self.web_pmi_columns)
            du.insert(web_pmi_rows)

        if len(web_pwmi_rows) > 0:
            print("Pages with matching images")
            du = db_utils(self.config, self.web_pwmi_table, self.web_pwmi_columns)
            du.insert(web_pwmi_rows)

        if len(web_vsi_rows) > 0:
            print("Visually similar images")
            du = db_utils(self.config, self.web_vsi_table, self.web_vsi_columns)
            du.insert(web_vsi_rows)

    def process_product_search_response(self, response):
        du = db_utils(self.config, self.product_search_table, self.product_search_columns)
        du.insert(response)

    def process_vision_data(self, image_rows):
        iu = image_utils(self.config, self.image_column_lst)
        print('Uploading images for annotation')
        dict_image_to_id = iu.upload_images(image_rows)

        response = iu.product_search(dict_image_to_id)
        self.process_product_search_response(response)

        print('Annotating images')
        results = iu.batch_annotate(dict_image_to_id)
        self.process_annotations(results)

    def process_content_enrichment(self, excel_rows):
        print(f'Populating {self.cef_table}')
        cef_rows = self.process_non_image_data(excel_rows)
        print(f'Added {len(cef_rows)} rows in  {self.cef_table}')

        print(f'Populating {self.image_table}')
        image_rows = self.process_image_data(cef_rows)
        print(f'Added {len(image_rows)} rows in  {self.image_table}')

        self.process_vision_data(image_rows)
