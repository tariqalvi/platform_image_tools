
import sys
import os
import configparser
from image_utilities import image_utils
from db_utilities import db_utils


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise SystemExit('Incorrect arguments!')

    config_file = sys.argv[1]
    if not os.path.exists(config_file):
            raise SystemExit(f'Config file {config_file} not found!')
    
    config = configparser.ConfigParser()
    config.read(config_file)

    try:
        os.environ['GOOGLE_PROJECT_ID'] = config['credentials']['GOOGLE_PROJECT_ID']
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config['credentials']['GOOGLE_APPLICATION_CREDENTIALS']
    except KeyError as e:
        raise SystemExit(f'Error {e} reading configuration')


    iu = image_utils(config)
    records = iu.batch_annotate()
    for record in records:
        print('===============')
        print(f'{record}')
        print('===============')

    du = db_utils(config)

