# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.common.exceptions import NoSuchElementException, TimeoutException
from google.cloud import vision_v1 as vision
import sys
import os
import configparser
import requests
import tldextract
import json
import time
import re

class FoodServiceDataCollector:
    def __init__(self, config):
        self.config = config
        self.vision_domains = {}
        self.sku_lst = FoodServiceDataCollector.extract_skus(config['default']['id_file'])
        # self.driver = webdriver.Chrome(self.config['default']['driver_location'])
        self.output_file = self.config['default']['results_file']
        self.page_load_wait = int(self.config['default']['page_load_wait'])
        self.aaa_wait = int(self.config['default']['aaa_wait'])
        self.aaa_timeout = int(self.config['default']['aaa_timeout'])
        self.aaa_apikey = self.config['default']['aaa_apikey']
        self.aaa_url = self.config['default']['aaa_url']
        self.generate_domains = False
        if 'generate_domains' in self.config['default']:
            self.generate_domains = bool(self.config['default']['generate_domains'].lower() == 'true')
        self.num_records = 0
        self.num_domains = 0

    # def wait_for_item(self, by, item):
    #     try:
    #         WebDriverWait(self.driver, self.page_load_wait).until(
    #             EC.presence_of_element_located((by, item)))
    #         return True
    #     except TimeoutException:
    #         return False
    #
    # def get_item(self, by, item):
    #     if self.wait_for_item(by, item) is False:
    #         return None
    #
    #     return self.driver.find_element(by, item)

    def update_domain_list(self):
        with open("domains.txt", 'w') as output_file_handle:
            for name, value in self.vision_domains.items():
                output_file_handle.write(f'{name},{value[0]},{value[1]}\n')
            
    def write_output(self, sku, zoro_img_url, external_pages, categories):
        if self.num_records == 0:
            with open(self.output_file, 'w', encoding="utf-8") as output_file_handle:
                output_file_handle.write('sku,image_url')
                i = 1
                for section in self.config:
                    if section.lower() != 'default':
                        output_file_handle.write(f',external_page_{i}')
                        i += 1

                for section in self.config:
                    if section.lower() != 'default':
                        tag = self.config[section]['tag']
                        for item in self.config[section]:
                            if item.startswith('level_'):
                                col_name = f'{tag}_{item}'
                                output_file_handle.write(',')
                                output_file_handle.write(f'{col_name}')
                output_file_handle.write('\n')

        with open(self.output_file, 'a', encoding="utf-8") as output_file_handle:
            if ',' in zoro_img_url:
                zoro_img_url = f'"{zoro_img_url}"'
            output_file_handle.write(f'{sku},{zoro_img_url}')
            for page in external_pages:
                output_file_handle.write(',')
                output_file_handle.write(f'{page}')

            for category in categories:
                output_file_handle.write(',')
                output_file_handle.write(f'{category}')

            output_file_handle.write('\n')

        self.num_records += 1
        print(f'Processed {sku}, Total processed {self.num_records} ')

    def process_sku_list(self):
        # self.driver.get(self.config['default']['start_url'])
        for sku, img_url in self.sku_lst:
            img_url = f'https://www.zoro.com/static/cms/product/full/{img_url}'
            # page_url, img_url = self.load_zoro_page(sku)
            categories = []
            external_pages = []
            if img_url:
                client = vision.ImageAnnotatorClient()
                image_source = vision.types.ImageSource(image_uri=img_url)
                image = vision.types.Image(source=image_source)
                print(f'Requesting web detection for {img_url}')
                annotations = client.web_detection(image=image).web_detection
                if annotations.pages_with_matching_images:
                    sections = self.config.sections()
                    for section in sections:
                        if section.lower() == 'default':
                            continue
                        domain_present = False
                        value_found = False
                        page_found = ''
                        for page in annotations.pages_with_matching_images:
                            ext = tldextract.extract(page.url)
                            if ext.suffix != 'com':
                                continue

                            domain = ext.registered_domain.lower()
                            if self.generate_domains:
                                if domain not in self.vision_domains:
                                    self.vision_domains[domain] = [1, page.url]
                                else:
                                    lst = self.vision_domains[domain]
                                    self.vision_domains[domain] = [lst[0] + 1, lst[1]]
                                self.update_domain_list()
                                continue

                            if domain.lower() == section.lower():
                                domain_present = True
                                body_dict = {'url': page.url}
                                selectors_dict = {}
                                sec_dict = dict(self.config.items(domain))
                                tag = sec_dict['tag']
                                for name, value in sec_dict.items():
                                    if name != 'tag':
                                        selectors_dict[name] = value
                                body_dict['selectors'] = selectors_dict
                                body_dict['wait'] = self.aaa_wait
                                body_dict['timeout'] = self.aaa_timeout
                                headers = {'Content-type': 'application/json', 'Accept': 'text/plain',
                                           'apikey': self.aaa_apikey}
                                print(f'Making AAA request for {body_dict}')
                                response = requests.post(self.aaa_url,
                                                         data=json.dumps(body_dict),
                                                         headers=headers)
                                if response.status_code != 200:
                                    print(f'Error in AAA request for {sku}, {response.status_code}, {response.reason}. '
                                          f'Are you connected to VPN?')
                                    continue

                                json_data = json.loads(response.text)
                                try:
                                    values = json_data['result'].values()
                                except Exception as e:
                                    print(e)

                                temp_cat = []
                                for value in values:
                                    if value:
                                        temp_cat.append(value)
                                        value_found = True
                                        page_found = page.url
                                    else:
                                        temp_cat.append('')
                                if value_found:
                                    categories.extend(temp_cat)
                                    break

                        if not domain_present or not value_found:
                            external_pages.append('')
                            cat_items = len(self.config.items(section)) - 1
                            x = range(cat_items)
                            for n in x:
                                categories.append('')
                        else:
                            external_pages.append(page_found)
                else:
                    print(f'No matching pages for {sku}')
            else:
                print(f'No image url found for {sku}')

            self.write_output(sku, img_url, external_pages, categories)

    # def load_zoro_page(self, sku):
    #     srch = self.get_item(By.XPATH, '//*[@id="app"]/div[2]/div/header/div/div[2]/form/input')
    #     if not srch:
    #         return '', ''
    #
    #     url_before = self.driver.current_url
    #     while srch.get_attribute('value') != '':
    #         srch.send_keys(Keys.BACKSPACE)
    #     srch.send_keys(sku)
    #     srch.send_keys(Keys.RETURN)
    #
    #     start = time.time()
    #     while self.driver.current_url == url_before:
    #         time.sleep(1)
    #         if time.time() - start > self.page_load_wait:
    #             return '', ''
    #
    #     if self.wait_for_item(By.XPATH,
    #                           '//*[@id="app"]/div[4]/div[2]/div/div/main/div[1]/div[3]/div/div[1]/div/div/div/div/'
    #                           'div/img') is False:
    #         return '', ''
    #
    #     page_url = self.driver.current_url
    #
    #     img_url = None
    #     start = time.time()
    #     while not img_url:
    #         source = self.driver.page_source
    #         soup = bs(source)
    #         page_url = self.driver.current_url
    #         img_div = soup.find("div", class_="hover-zoom-image product-images__hover-zoom")
    #         img_url = img_div.find('img')['src']
    #         if 'ajax-loader' in img_url:
    #             print('Image loading....')
    #             img_url = None
    #             time.sleep(1)
    #             if time.time() - start > self.page_load_wait:
    #                 return '', ''
    #
    #     print(f'Image loaded {img_url}')
    #     return page_url, img_url

    @staticmethod
    def extract_skus(id_file):
        sku_lst = []
        with open(id_file, encoding='utf-8-sig') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip()
                if '"' in line:
                    values = re.split(r',(?=")', line)
                else:
                    values = line.split(',')
                if len(values) != 2 or values[1] == '0':
                    continue
                sku_lst.append((values[0], values[1].strip('"')))
        return sku_lst


if __name__ == "__main__":
    num_args = len(sys.argv)
    if num_args < 2:
        raise SystemExit('Config file not supplied!')

    config_file = sys.argv[1]
    if not os.path.exists(config_file):
        raise SystemExit(f'Config file {config_file} not found!')

    config = configparser.ConfigParser()
    config.read(config_file)

    fsdc = FoodServiceDataCollector(config)
    fsdc.process_sku_list()
