import time
import apache_beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from apache_beam.io.textio import ReadFromText, WriteToText
from collections import OrderedDict
import re
import configparser
import requests
import tldextract
import json
from google.cloud import vision_v1 as vision
import logging

missing_domains_filename= 'missing_domains.txt'
missing_breadcrumbs_filename= 'missing_breadcrumbs.txt'
config_file = 'automation.ini'
input_filename = 'gs://zoro-dev-dataflow-artifacts/tariq/fsap.csv'
output_filename = 'gs://zoro-dev-dataflow-artifacts/tariq/fsap_out.txt'
dataflow_options = ['--project=z0r0-dev-service', '--job_name=fsap', '--temp_location=gs://zoro-dev-dataflow-artifacts/tariq/temp']
dataflow_options.append('--staging_location=gs://zoro-dev-dataflow-artifacts/tariq/stage')
dataflow_options.append('--service_account_email=dataflow-gcp-engineers-controller-sa@z0r0-dev-service.iam.gserviceaccount.com')
dataflow_options.append('--service_account_email=dataflow-gcp-eng-controller-sa@z0r0-dev-service.iam.gserviceaccount.com')
dataflow_options.append('--no_use_public_ips')
dataflow_options.append('--subnetwork=regions/us-east1/subnetworks/dev-subnet')
dataflow_options.append('--subnetwork=https://www.googleapis.com/compute/v1/projects/z0r0-dev-host/regions/us-east1/subnetworks/dev-subnet')
dataflow_options.append('--region=us-east1')
dataflow_options.append('--worker_region=us-east1')
dataflow_options.append('--save_main_session')
dataflow_options.append('--requirements_file=requirements.txt')
dataflow_options.append('--max_num_workers=1')

options = PipelineOptions(dataflow_options)
gcloud_options = options.view_as(GoogleCloudOptions)
options.view_as(StandardOptions).runner = 'dataflow'


class Split(apache_beam.DoFn):
    def __init__(self, config):
        self.config = config

    def process(self, element):
        line = element.strip()
        if '"' in line:
            values = re.split(r',(?=")', line)
        else:
            values = line.split(',')

        sku = values[0]
        url = values[1].strip('"')
        img_url = f'https://www.zoro.com/static/cms/product/full/{url}'
        client = vision.ImageAnnotatorClient()
        image_source = vision.types.ImageSource(image_uri=img_url)
        image = vision.types.Image(source=image_source)
        logging.info(f'Requesting web detection for {img_url}')
        annotations = client.web_detection(image=image).web_detection
        ret = {'sku': sku, 'img_url': img_url}
        if annotations and annotations.pages_with_matching_images:
            page_lst = []
            for page in annotations.pages_with_matching_images:
                page_lst.append(page.url)

            if not page_lst:
                print(f'No matching pages detected for {sku}')
                return None

            ret['pages'] = page_lst
        # print(f'Web detection {ret}')
        return [ret]

class ProcessPages(apache_beam.DoFn):
    def __init__(self, config):
        self.config = config
        self.aaa_wait = int(self.config['default']['aaa_wait'])
        self.aaa_timeout = int(self.config['default']['aaa_timeout'])
        self.aaa_apikey = self.config['default']['aaa_apikey']
        self.aaa_url = self.config['default']['aaa_url']

    def query_aaa(self, page, domain, sec_dict):
        body_dict = {'url': page}
        selectors_dict = {}
        sec_dict = dict(self.config.items(domain))
        tag = sec_dict['tag']
        for name, value in sec_dict.items():
            if name != 'tag':
                selectors_dict[name] = value
        body_dict['selectors'] = selectors_dict
        body_dict['wait'] = self.aaa_wait
        body_dict['timeout'] = self.aaa_timeout
        # headers = {'Content-type': 'application/json', 'Accept': 'text/plain',
        #           'apikey': self.aaa_apikey}
        headers = {'apikey': self.aaa_apikey,
                   'Content-type': 'application/json',
                   'User-Agent': 'PostmanRuntime/7.26.8',
                   'Accept': '*/*',
                   'Cache-Control': 'no-cache',
                   'Postman-Token': 'a019ea35-7b1b-463b-802e-27ef95d9abca',
                   'Host': 'api.dev.zoro.com',
                   'Accept-Encoding': 'gzip, deflate, br',
                   'Connection': 'keep-alive',
                   'Content-Length': '228' }
        # print(f'Making AAA request for {body_dict}')
        start = time.time()
        logging.info(f'AAA query {page}')
        values = ['', '', '', '', '', '', '']
        response = requests.post(self.aaa_url,
                                 data=json.dumps(body_dict),
                                 headers=headers)
        if response.status_code != 200:
            logging.info(f'Error in AAA Request: Time delta: {time.time() - start}\n{body_dict}\nHeaders: {headers}\nResponse headers: {response.headers}\nStatus code: {response.status_code}\nReason {response.reason}. '
                  f'Are you connected to VPN?')
            return values

        json_data = json.loads(response.text)

        try:
            result_vals = list(json_data['result'].values())
            if result_vals:
                values = [x if x else '' for x in result_vals]
        except Exception as e:
            logging.info(e)

        logging.info(f'AAA query response {values}')
        return values

    def process(self, element):
        sections = self.config.sections()
        ret = {'sku': element['sku'], 'img_url': element['img_url'], 'tags': {}}
        has_matching_domain = False
        bread_crumbs_found = False
        if 'pages' in element:
            for section in sections:
                if section.lower() == 'default':
                    continue
                tag = self.config[section]['tag']
                largest_breadcrumb_size = 0
                for page in element['pages']:
                    ext = tldextract.extract(page)
                    if ext.suffix != 'com':
                        continue
                    domain = ext.registered_domain.lower()
                    if domain.lower() == section.lower():
                        has_matching_domain = True
                        sec_dict = dict(self.config.items(domain))
                        values = self.query_aaa(page, domain, sec_dict)
                        num_nonzero = sum(x != '' for x in values)
                        if num_nonzero == 0:
                            ProcessPages.add_missing_breadcrumbs(element['sku'], page, domain)
                        if num_nonzero == 0 and f'{domain}_secondary' in self.config:
                            sec_dict = dict(self.config.items(f'{domain}_secondary'))
                            values = self.query_aaa(page, f'{domain}_secondary', sec_dict)
                            num_nonzero = sum(x != '' for x in values)
                            if num_nonzero == 0:
                                ProcessPages.add_missing_breadcrumbs(element['sku'], page, f'{domain}_secondary')

                        if num_nonzero >= largest_breadcrumb_size:
                            ret['tags'][tag] = {'page': page, 'breadcrumbs': values}
                            largest_breadcrumb_size = num_nonzero
                            if largest_breadcrumb_size > 0:
                                bread_crumbs_found = True
        if not has_matching_domain:
            logging.info(f'No matching domain for {element["sku"]}')
            pages = ''
            if 'pages' in element:
                pages = element['pages']
            ProcessPages.add_missing_domains(element['sku'], pages)
            return None

        if not bread_crumbs_found:
            logging.info(f'No breadcrumbs found for {element["sku"]}')
            return None

        logging.info(f'Found breadcrumbs for {element["sku"]}')
        return [ret]

    @staticmethod
    def add_missing_domains(sku, pages):
        with open(missing_domains_filename, 'a') as output_file_handle:
            output_file_handle.write(f'{sku},{pages}\n')

    @staticmethod
    def add_missing_breadcrumbs(sku, page, domain):
        with open(missing_breadcrumbs_filename, 'a') as output_file_handle:
            output_file_handle.write(f'{sku},{page},{domain}\n')

class Record:
    def __init__(self, config, sku, img_url):
        self.record = OrderedDict()
        self.record = {'sku': sku, 'img_url': img_url, 'tags': {}}
        for section in config:
            if section.lower() != 'default':
                tag = config[section]['tag']

                self.record['tags'][tag] = {'page': '', 'values': ['', '', '', '', '', '', '']}


    def add_or_update(self, tag, page, values):
        num_nonzero_stored = sum(x != '' for x in self.record['tags'][tag]['values'])
        num_nonzero_values = sum(x != '' for x in values)
        if num_nonzero_values >= num_nonzero_stored:
            self.record['tags'][tag]['page'] = page
            self.record['tags'][tag]['values'] = values

    def header(self):
        output = 'sku,img'
        for tag in self.record['tags'].keys():
            output = f'{output},page_{tag},crumb_{tag}_1,crumb_{tag}_2,crumb_{tag}_3,crumb_{tag}_4,crumb_{tag}_5,crumb_{tag}_6,crumb_{tag}_7'
        return output

    def row(self):
        zoro_img_url = self.record['img_url']
        if ',' in zoro_img_url:
            zoro_img_url = f'"{zoro_img_url}"'
        output = f'{self.record["sku"]},{zoro_img_url}'
        tags = self.record['tags']
        for attribute in tags.values():
            crumb_string = ','.join([elem for elem in attribute['values']])
            output = f'{output},{attribute["page"]},{crumb_string}'

        return output


class WriteOutput(apache_beam.DoFn):
    def __init__(self, config):
        self.config = config
        self.num_records = 0

    def process(self, element):
        record = Record(self.config, element['sku'], element['img_url'])
        element_tags = element['tags']
        for section in self.config:
            if section.lower() != 'default':
                config_tag = self.config[section]['tag']
                if config_tag in element_tags:
                    element_tag_dict = element_tags[config_tag]
                    bread_crumbs = element_tag_dict['breadcrumbs']
                    url = element_tag_dict['page']
                    record.add_or_update(config_tag, url, bread_crumbs)

        output_string = ''
        if self.num_records == 0:
            output_string = f'{record.header()}\n'
        output_string = f'{output_string}{record.row()}'

        # logging.info(f'output_string = {output_string}')
        self.num_records += 1
        return [output_string]


def main():
    parser = configparser.ConfigParser()
    parser.read(config_file)
    logging.info(dict(parser.items('default')))
    with apache_beam.Pipeline(options=options) as p:
        print('Waiting.....')
        (p | ReadFromText(input_filename) |
         apache_beam.ParDo(Split(parser)) |
         apache_beam.ParDo(ProcessPages(parser)) |
         apache_beam.ParDo(WriteOutput(parser)) |
         WriteToText(output_filename, file_name_suffix='.txt'))


if __name__ == "__main__":
    main()

