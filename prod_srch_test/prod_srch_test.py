import sys
import os
import re
import logging
import traceback
from google.cloud import vision


logger = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
logger.addHandler(out_hdlr)
logger.setLevel(logging.INFO)


def get_similar_products_file(
        project_id, location, product_set_id, product_category,
        file_path, filter):
    """Search similar products to image.
    Args:
        project_id: Id of the project.
        location: A compute region name.
        product_set_id: Id of the product set.
        product_category: Category of the product.
        file_path: Local file path of the image to be searched.
        filter: Condition to be applied on the labels.
        Example for filter: (color = red OR color = blue) AND style = kids
        It will search on all products with the following labels:
        color:red AND style:kids
        color:blue AND style:kids
    """
    # product_search_client is needed only for its helper methods.
    product_search_client = vision.ProductSearchClient()
    image_annotator_client = vision.ImageAnnotatorClient()

    # Read the image as a stream of bytes.
    with open(file_path, 'rb') as image_file:
        content = image_file.read()

    # Create annotate image request along with product search feature.
    image = vision.types.Image(content=content)

    # product search specific parameters
    product_set_path = product_search_client.product_set_path(
        project=project_id, location=location,
        product_set=product_set_id)
    product_search_params = vision.types.ProductSearchParams(
        product_set=product_set_path,
        product_categories=[product_category],
        filter=filter)
    image_context = vision.types.ImageContext(
        product_search_params=product_search_params)

    # Search products similar to the image.
    response = image_annotator_client.product_search(
        image, image_context=image_context)

    index_time = response.product_search_results.index_time
    print('Product set index time:')
    print('  seconds: {}'.format(index_time.seconds))
    print('  nanos: {}\n'.format(index_time.nanos))

    results = response.product_search_results.results

    print('Search results:')
    for result in results:
        product = result.product

        print('Score(Confidence): {}'.format(result.score))
        print('Image name: {}'.format(result.image))

        print('Product name: {}'.format(product.name))
        print('Product display name: {}'.format(
            product.display_name))
        print('Product description: {}\n'.format(product.description))
        print('Product labels: {}\n'.format(product.product_labels))

if __name__ == "__main__":
    logger.info('')
    logger.info('')
    logger.info('')
    get_similar_products_file('zoro-platform-sb', 'us-west1', 'zoro-products', 'general-v1', 'hardhat.jpeg', None)
    logger.info('==========================')
    get_similar_products_file('zoro-platform-sb', 'us-west1', 'zoro-products', 'general-v1', 'gloves.jpg', None)
    logger.info('==========================')
    get_similar_products_file('zoro-platform-sb', 'us-west1', 'zoro-products', 'general-v1', 'battery.jpeg', None)
    logger.info('==========================')
    get_similar_products_file('zoro-platform-sb', 'us-west1', 'zoro-products', 'general-v1', 'hammer.jpg', None)
    logger.info('==========================')
    get_similar_products_file('zoro-platform-sb', 'us-west1', 'zoro-products', 'general-v1', 'thermometer.jpeg', None)
