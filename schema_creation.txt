CREATE SEQUENCE public.id_seq;
CREATE TABLE public.content_enrichment (
id rr DEFAULT nextval('public.id_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
supplier_id_num text NOT NULL,
supplier_short_name text NOT NULL,
zoro_num text,
mfr_model_num text,
supplier_stock_num text NOT NULL,
ind_srch_kwds text,
pri_app_ind text,
msrp numeric,
rel_id_1 text,
rel_type_1 text,
rel_id_2 text,
rel_type_2 text,
rel_id_3 text,
rel_type_3 text,
rel_id_4 text,
rel_type_4 text,
rel_id_5 text,
rel_type_5 text,
sell_feat_1 text,
sell_feat_2 text,
sell_feat_3 text,
sell_feat_4 text,
sell_feat_5 text,
material text,
color text,
width numeric,
length numeric,
height numeric,
depth numeric,
ov_length numeric,
ov_height numeric,
ov_width numeric,
in_dmtr numeric,
out_dmtr numeric,
weight numeric,
includes text,
fuw text,
features text,
app text,
finish text,
volts text,
size text,
thkns numeric,
capacity text,
stdrds text,
att_nm_1 text,
att_val_1 text,
att_nm_2 text,
att_val_2 text,
att_nm_3 text,
att_val_3 text,
att_nm_4 text,
att_val_4 text,
att_nm_5 text,
att_val_5 text,
att_nm_6 text,
att_val_6 text,
att_nm_7 text,
att_val_7 text,
att_nm_8 text,
att_val_8 text,
att_nm_9 text,
att_val_9 text,
att_nm_10 text,
att_val_10 text,
att_nm_11 text,
att_val_11 text,
att_nm_12 text,
att_val_12 text,
att_nm_13 text,
att_val_13 text,
att_nm_14 text,
att_val_14 text,
att_nm_15 text,
att_val_15 text
);
ALTER TABLE
  public.content_enrichment
ADD
  PRIMARY KEY (id);
CREATE UNIQUE INDEX sin_ssn ON public.content_enrichment (supplier_short_name, supplier_stock_num);

CREATE OR REPLACE FUNCTION update_changetimestamp_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.modify_time = now(); 
   RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON content_enrichment FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();


    ===============
CREATE SEQUENCE public.images_id_seq;
CREATE TABLE public.images (
id integer DEFAULT nextval('public.images_id_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
cef_id INTEGER REFERENCES content_enrichment(id),
asset text,
asset_type text
);

ALTER TABLE
  public.images
ADD
  PRIMARY KEY (id);
CREATE UNIQUE INDEX index_asset ON public.images (cef_id, asset);

================
CREATE SEQUENCE public.miising_images_id_seq;
CREATE TABLE public.missing_images (
id integer DEFAULT nextval('public.miising_images_id_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
supplier_short_name text,
supplier_stock_num text,
asset text,
scrape_sites text,
scrape_allow_expressions text
);

ALTER TABLE
  public.missing_images
ADD
  PRIMARY KEY (id);
CREATE UNIQUE INDEX index_missing_asset ON public.missing_images (supplier_short_name, asset);
============

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON images FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();

=============================
CREATE SEQUENCE public.label_annotations_id_seq;
CREATE TABLE public.label_annotations (
id integer DEFAULT nextval('public.label_annotations_id_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
description text,
mid text,
score numeric,
topicality numeric
);

ALTER TABLE
  public.label_annotations
ADD
  PRIMARY KEY (id);
CREATE UNIQUE INDEX index_mid ON public.label_annotations (mid);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON label_annotations FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
===================================================

CREATE SEQUENCE public.label_annotations_id_seq;
CREATE TABLE public.label_annotations (
id integer DEFAULT nextval('public.label_annotations_id_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
description text,
mid text,
score numeric,
topicality numeric
);

ALTER TABLE
  public.label_annotations
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON label_annotations FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
==========================
CREATE SEQUENCE public.bglabels_seq;
CREATE TABLE public.web_best_guess_labels (
id integer DEFAULT nextval('public.bglabels_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
Label text
);

ALTER TABLE
  public.web_best_guess_labels
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON web_best_guess_labels FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
==========================================
CREATE SEQUENCE public.webentities_seq;
CREATE TABLE public.web_entities (
id integer DEFAULT nextval('public.webentities_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
description text,
entityId text,
score numeric
);

ALTER TABLE
  public.web_entities
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON web_entities FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
    ================
CREATE SEQUENCE public.webfmi_seq;
CREATE TABLE public.web_full_matching_images (
id integer DEFAULT nextval('public.webfmi_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
url text
);

ALTER TABLE
  public.web_full_matching_images
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON web_full_matching_images FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
    ================
CREATE SEQUENCE public.webpmi_seq;
CREATE TABLE public.web_partial_matching_images (
id integer DEFAULT nextval('public.webpmi_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
url text
);

ALTER TABLE
  public.web_partial_matching_images
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON web_partial_matching_images FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
=================
CREATE SEQUENCE public.webpmwi_seq;
CREATE TABLE public.web_pages_with_matching_images (
id integer DEFAULT nextval('public.webpmwi_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
pagetitle text,
url text,
partialmatchingimages text
);

ALTER TABLE
  public.web_pages_with_matching_images
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON web_pages_with_matching_images FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
===========
CREATE SEQUENCE public.webvsi_seq;
CREATE TABLE public.web_visually_similar_images (
id integer DEFAULT nextval('public.webvsi_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
url text
);

ALTER TABLE
  public.web_visually_similar_images
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON web_visually_similar_images FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
=============
CREATE SEQUENCE public.ss_seq;
CREATE TABLE public.safe_search (
id integer DEFAULT nextval('public.ss_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
adult text,
spoof text,
medical text,
violence text,
racy text
);

ALTER TABLE
  public.safe_search
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON safe_search FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
=====================
CREATE SEQUENCE public.pc_seq;
CREATE TABLE public.product_search_suggestions (
id integer DEFAULT nextval('public.pc_seq'),
create_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp with time zone,
images_id INTEGER REFERENCES images(id),
reference_image text,
reference_name text,
reference_display_name text,
reference_description text,
reference_title text,
reference_categoryL1_name text,
reference_categoryL2_name text,
reference_categoryL3_name text,
reference_categoryL1_code INTEGER,
reference_categoryL2_code INTEGER,
reference_categoryL3_code INTEGER,
score numeric
);

ALTER TABLE
  public.product_search_suggestions
ADD
  PRIMARY KEY (id);

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
    ON product_search_suggestions FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();